﻿-- Table: public.cidades

-- DROP TABLE public.cidades;

CREATE TABLE public.cidades
(
  idcidade serial,
  cep character varying(8),
  nomecidade character varying(50) NOT NULL,
  uf character(2),
  CONSTRAINT pkcidade PRIMARY KEY (idcidade)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.cidades
  OWNER TO postgres;

-- Table: public.interfaces

-- DROP TABLE public.interfaces;

CREATE TABLE public.interfaces
(
  idinterface serial,
  nomeinterface character varying(100),
  CONSTRAINT pkinterface PRIMARY KEY (idinterface)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.interfaces
  OWNER TO postgres;

-- Table: public.grupos

-- DROP TABLE public.grupos;

CREATE TABLE public.grupos
(
  idgrupo serial,
  descricao character varying(45),
  CONSTRAINT fkgrupo PRIMARY KEY (idgrupo)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.grupos
  OWNER TO postgres;

-- Table: public.grupointerface

-- DROP TABLE public.grupointerface;

CREATE TABLE public.grupointerface
(
  idgrupointerface serial,
  idgrupo integer NOT NULL,
  idinterface integer NOT NULL,
  CONSTRAINT pkidgrupointerface PRIMARY KEY (idgrupointerface),
  CONSTRAINT fkgrupo FOREIGN KEY (idgrupo)
      REFERENCES public.grupos (idgrupo) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkidinterface FOREIGN KEY (idinterface)
      REFERENCES public.interfaces (idinterface) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.grupointerface
  OWNER TO postgres;

-- Index: public.fki_fkidinterface

-- DROP INDEX public.fki_fkidinterface;

CREATE INDEX fki_fkidinterface
  ON public.grupointerface
  USING btree
  (idinterface);


-- Table: public.usuarios

-- DROP TABLE public.usuarios;

CREATE TABLE public.usuarios
(
  idusuario serial,
  nome character varying(40) NOT NULL,  
  login character varying(30) NOT NULL,
  senha character varying(30) NOT NULL,
  status character varying(1) NOT NULL,
  idgrupo integer,
  CONSTRAINT pkusuario PRIMARY KEY (idusuario),
  CONSTRAINT fkgrupo FOREIGN KEY (idgrupo)
      REFERENCES public.grupos (idgrupo) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.usuarios
  OWNER TO postgres;

-- Table: public.tipoeventos

-- DROP TABLE public.tipoeventos;

CREATE TABLE public.tipoeventos
(
  idtipoevento serial,
  descricao character varying(60) NOT NULL,
  CONSTRAINT pktipoevento PRIMARY KEY (idtipoevento)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.tipoeventos
  OWNER TO postgres;

-- Table: public.fornecedores"

-- DROP TABLE public.fornecedores;

CREATE TABLE public.fornecedores
(
  idFornecedor serial,
  nome character varying(60) NOT NULL,
  cnpj character varying(17) NOT NULL UNIQUE,
  email character varying(40) NOT NULL,
  telefone character varying(16) NOT NULL,
  telefone2 character varying(16) NOT NULL,
  idtipoevento integer,
  idcidade integer,
  CONSTRAINT pkfornecedor PRIMARY KEY (idFornecedor),
  CONSTRAINT fkcidade FOREIGN KEY (idcidade)
      REFERENCES public.cidades (idcidade) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT pktipoevento FOREIGN KEY (idtipoevento)
      REFERENCES public.tipoeventos (idtipoevento) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.fornecedores
  OWNER TO postgres;

-- Table: public.locais

-- DROP TABLE public.locais;

CREATE TABLE public.locais
(
  idlocal serial,
  idfornecedor integer NOT NULL,
  descricao character varying(60) NOT NULL,
  CONSTRAINT pklocal PRIMARY KEY (idlocal),
  CONSTRAINT fkfornecedor FOREIGN KEY (idfornecedor)
      REFERENCES public.fornecedores (idfornecedor) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.locais
  OWNER TO postgres;



-- Table: public.setores

-- DROP TABLE public.setores;

CREATE TABLE public.setores
(
  idsetor serial,
  idlocal integer NOT NULL,
  descricao character varying(60) NOT NULL,
  capacidade integer NOT NULL,
  CONSTRAINT pksetor PRIMARY KEY (idsetor),
  CONSTRAINT fklocal FOREIGN KEY (idlocal)
      REFERENCES public.locais (idlocal) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.setores
  OWNER TO postgres;

-- Table: public.eventos

-- DROP TABLE public.eventos;

CREATE TABLE public.eventos
(
  idevento serial,
  descricao character varying(100) NOT NULL,
  dataevento character varying(10) NOT NULL,
  horaevento character varying(5) NOT NULL,
  totalingressos integer NOT NULL,
  idfornecedor integer NOT NULL,
  idlocal integer NOT NULL,
  CONSTRAINT pkevento PRIMARY KEY (idevento),
  CONSTRAINT fkfornecedor FOREIGN KEY (idfornecedor)
      REFERENCES public.fornecedores (idfornecedor) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fklocal FOREIGN KEY (idlocal)
      REFERENCES public.locais (idlocal) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.eventos
  OWNER TO postgres;

-- Table: public.ingressos

-- DROP TABLE public.ingressos;

CREATE TABLE public.ingressos
(
  idingresso serial,
  quantidade integer,
  precounitario double precision NOT NULL,
  idsetor integer NOT NULL,
  idevento integer NOT NULL,
  CONSTRAINT pkingresso PRIMARY KEY (idingresso),
  CONSTRAINT fkevento FOREIGN KEY (idevento)
      REFERENCES public.eventos (idevento) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fksetorlocal FOREIGN KEY (idsetor)
      REFERENCES public.setores (idsetor) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT ingressos_idsetor_key UNIQUE (idsetor)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.ingressos
  OWNER TO postgres;

-- Table: public.vendas

-- DROP TABLE public.vendas;

CREATE TABLE public.vendas
(
  idvenda serial,
  datavenda date NOT NULL,
  valortotal double precision NOT NULL,
  idevento integer NOT NULL,
  idingresso integer,
  qtde integer,
  CONSTRAINT pkvenda PRIMARY KEY (idvenda),
  CONSTRAINT fkevento FOREIGN KEY (idevento)
      REFERENCES public.eventos (idevento) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.vendas
  OWNER TO postgres;


-- Table: public.tabelaauditoria

-- DROP TABLE public.tabelaauditoria;

CREATE TABLE public.tabelaauditoria
(
  idtabela serial,
  tabela character varying(30) NOT NULL,
  habilitado character varying(3) NOT NULL,
  CONSTRAINT pktabela PRIMARY KEY (idtabela)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.tabelaauditoria
  OWNER TO postgres;


-- Table: public.auditorias

-- DROP TABLE public.auditorias;

CREATE TABLE public.auditorias
(
  idauditoria serial,
  idusuarioauditoria integer NOT NULL,
  dataauditoria character varying(19) NOT NULL,
  tipoauditoria character varying(3) NOT NULL,
  idtabelaauditoria integer NOT NULL,
  valorauditoria character varying(2000) NOT NULL,
  CONSTRAINT pkauditoria PRIMARY KEY (idauditoria),
  CONSTRAINT fktblauditoria FOREIGN KEY (idtabelaauditoria)
      REFERENCES public.tabelaauditoria (idtabela) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkusuario FOREIGN KEY (idusuarioauditoria)
      REFERENCES public.usuarios (idusuario) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.auditorias
  OWNER TO postgres;

-- Table: public.log

-- DROP TABLE public.log;

CREATE TABLE public.log
(
  idlog serial,
  idusuariolog integer NOT NULL,
  datalog character varying(19) NOT NULL,
  idtabelalog integer NOT NULL,
  valor character varying(2000) NOT NULL,
  CONSTRAINT pklog PRIMARY KEY (idlog),
  CONSTRAINT fktabelalog FOREIGN KEY (idtabelalog)
      REFERENCES public.tabelaauditoria (idtabela) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fkusuariolog FOREIGN KEY (idusuariolog)
      REFERENCES public.usuarios (idusuario) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.log
  OWNER TO postgres;

-- View: public.vw_auditorias

-- DROP VIEW public.vw_auditorias;

CREATE OR REPLACE VIEW public.vwauditorias AS 
 SELECT a.idauditoria,
    a.idusuarioauditoria,
    u.nome AS nomeusuario,
    u.login,
    a.dataauditoria,
    a.tipoauditoria,
    a.idtabelaauditoria,
    t.tabela,
    a.valorauditoria
   FROM auditorias a,
    tabelaauditoria t,
    usuarios u
  WHERE t.idtabela = a.idtabelaauditoria AND a.idusuarioauditoria = u.idusuario;

ALTER TABLE public.vwauditorias
  OWNER TO postgres;

-- View: public.vwlogs

-- DROP VIEW public.vwlogs;

CREATE OR REPLACE VIEW public.vwlogs AS 
 SELECT a.idlog,
    a.idusuariolog,
    u.nome AS nomeusuario,
    u.login,
    a.datalog,
    a.idtabelalog,
    t.tabela AS tabelalog,
    a.valor
   FROM log a,
    tabelaauditoria t,
    usuarios u
  WHERE t.idtabela = a.idtabelalog and a.idusuariolog = u.idusuario;

ALTER TABLE public.vwlogs
  OWNER TO postgres;



/*************************************************************************************************/




CREATE OR REPLACE FUNCTION diminuiIngresso (qtde INTEGER, evento INTEGER, setor INTEGER)
RETURNS INTEGER
	AS $$
BEGIN
	Update Ingressos set
	quantidade = quantidade - qtde
	WHERE idsetor = setor
	AND idevento = evento;
RETURN 0;

END;

$$ LANGUAGE plpgsql;
 
/*SELECT diminuiIngresso (2, 1, 1);*/




/*************************************************************************************************/




CREATE OR REPLACE FUNCTION f_venda_ingresso ()
RETURNS TRIGGER
	AS $$
DECLARE
	setor INTEGER;
	ig INTEGER;
	qtd INTEGER;
	evento INTEGER;
BEGIN
	ig := NEW.idingresso;
	qtd := NEW.qtde;
	
	SELECT idsetor, idevento INTO setor, evento
	FROM ingressos 
	WHERE idingresso = ig;

	PERFORM diminuiIngresso (qtd, evento, setor);
	
	RETURN NEW;
END;

$$ LANGUAGE plpgsql;

CREATE TRIGGER t_venda_ingresso
AFTER INSERT ON vendas
FOR EACH ROW
EXECUTE PROCEDURE f_venda_ingresso ();

/*
INSERT INTO vendas VALUES (2,'2016-11-15',600.00,1,1,2);
*/




/*************************************************************************************************/




CREATE OR REPLACE FUNCTION acrescentaCapacidade (cap INTEGER, evento INTEGER, setor INTEGER)
RETURNS INTEGER
	AS $$
BEGIN
	Update Ingressos set
	quantidade = cap
	WHERE idsetor = setor
	AND idevento = evento;
RETURN 0;

END;

$$ LANGUAGE plpgsql;
 
/*SELECT acrescentaCapacidade (2000, 1, 1);*/




/*************************************************************************************************/




CREATE OR REPLACE FUNCTION f_acrescentaCapacidade ()
RETURNS TRIGGER
	AS $$
DECLARE
	setor INTEGER;
	cp INTEGER;
	evento INTEGER;
BEGIN
	setor := NEW.idsetor;
	evento := NEW.idevento;
	
	SELECT capacidade INTO cp
	FROM setores
	WHERE idsetor = setor;

	PERFORM acrescentaCapacidade (cp, evento, setor);
	
	RETURN NEW;
END;

$$ LANGUAGE plpgsql;

CREATE TRIGGER t_acrescentaCapacidade
AFTER INSERT ON ingressos
FOR EACH ROW
EXECUTE PROCEDURE f_acrescentaCapacidade ();

-- View: public.vwvendas

-- DROP VIEW public.vwvendas;

CREATE OR REPLACE VIEW public.vwvendas AS 
 SELECT e.descricao,
    e.idevento,
    e.dataevento,
    e.horaevento,
    l.descricao AS local,
    v.datavenda,
    s.descricao AS setor,
    sum(v.qtde) AS vendidos,
    sum(v.valortotal) AS valortotal
   FROM vendas v,
    ingressos i,
    eventos e,
    locais l,
    setores s
  WHERE i.idevento = v.idevento AND i.idingresso = v.idingresso AND e.idevento = v.idevento AND l.idlocal = e.idlocal AND s.idsetor = i.idsetor
  GROUP BY e.descricao, e.idevento, e.dataevento, e.horaevento, v.datavenda, l.descricao, s.descricao
  ORDER BY e.idevento, v.datavenda;

ALTER TABLE public.vwvendas
  OWNER TO postgres;
