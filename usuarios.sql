﻿-- Table: public.permissoes

-- DROP TABLE public.permissoes;

CREATE TABLE public.permissoes
(
  idpermissao integer NOT NULL DEFAULT nextval('permissao_idpermissao_seq'::regclass),
  descricao character(45),
  interface character(60),
  CONSTRAINT pkpermissao PRIMARY KEY (idpermissao)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.permissoes
  OWNER TO postgres;

-- Table: public.grupousuario

-- DROP TABLE public.grupousuario;

CREATE TABLE public.grupousuario
(
  idgrupo integer NOT NULL DEFAULT nextval('grupopermissao_idgrupo_seq'::regclass),
  descricao character(45),
  idpermissao integer,
  CONSTRAINT pkgrupo PRIMARY KEY (idgrupo),
  CONSTRAINT fkpermissao FOREIGN KEY (idpermissao)
      REFERENCES public.permissoes (idpermissao) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.grupousuario
  OWNER TO postgres;
-- Table: public.usuarios

-- DROP TABLE public.usuarios;

CREATE TABLE public.usuarios
(
  idusuario integer NOT NULL DEFAULT nextval('usuarios_idusuario_seq'::regclass),
  senha character(30),
  login character(30),
  status character(1),
  nome character(60),
  idgrupo integer NOT NULL DEFAULT nextval('usuarios_idgrupo_seq'::regclass),
  CONSTRAINT pkusuario PRIMARY KEY (idusuario),
  CONSTRAINT fkgrupo FOREIGN KEY (idgrupo)
      REFERENCES public.grupousuario (idgrupo) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.usuarios
  OWNER TO postgres;
