/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;

/**
 *
 * @author ivan.stroher
 */
import javax.swing.JOptionPane;

public class Util {

    public static void Aviso(String msg) 
    {
        JOptionPane.showMessageDialog(null,msg, "Aviso", JOptionPane.INFORMATION_MESSAGE);
    }
    
    public static boolean Confirma(String msg) 
    {
        Object[] botoes={"sim","não"};
        return JOptionPane.showOptionDialog (null,msg, "Fechar o aplicativo",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE,null,botoes,botoes[0]) == JOptionPane.YES_OPTION;
    }
}