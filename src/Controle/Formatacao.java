package Controle;

import static Controle.Formatacao.df;
import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.*;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import javax.swing.*;
import javax.swing.text.*;

public class Formatacao {

    static DecimalFormat df = new DecimalFormat("#,##0.00", new DecimalFormatSymbols(new Locale("pt", "BR")));

    public static JFormattedTextField getFormatado(String formato) {
        JFormattedTextField campoFormatado = null;
        MaskFormatter format = new MaskFormatter();

        format.setPlaceholderCharacter(' ');
        format.setValueContainsLiteralCharacters(false);

        try {
            format.setMask(formato);
            campoFormatado = new JFormattedTextField(format);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return campoFormatado;
    }

    public static void formatarDecimal(JTextField campo) {
        campo.setText(df.format(Double.parseDouble(campo.getText())));
    }

    public static JFormattedTextField getTelefone() {
        return getFormatado("(##) ####-####");
    }

    public static JFormattedTextField getCNPJ() {
        return getFormatado("##.###.###/####-##");
    }

    public static JFormattedTextField getCPF() {
        return getFormatado("###.###.###-##");
    }

    public static JFormattedTextField getData() {
        return getFormatado("##/##/####");
    }

    public static JFormattedTextField getHora() {
        return getFormatado("##:##");
    }
        
    public static JFormattedTextField getCep() {
        return getFormatado("#####-###");
    }

    public void formatoDecimal(JTextField campo) {
        campo.setText(df.format(Double.parseDouble(campo.getText())));
    }

    public static void alinhaADireita(JTextField campo) {
        campo.setHorizontalAlignment(campo.RIGHT);
    }
      
    public static void alinhaCentro(JTextField campo) {
        campo.setHorizontalAlignment(campo.CENTER);
    }

    public static void reformatarData(JFormattedTextField campo) {
        try {
            MaskFormatter m = new MaskFormatter();
            m.setPlaceholderCharacter(' ');
            m.setMask("##/##/####");
            campo.setFormatterFactory(null);
            campo.setFormatterFactory(new DefaultFormatterFactory(m));
            campo.setValue(null);
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public static void reformatarCpf(JFormattedTextField campo) {
        try {
            MaskFormatter m = new MaskFormatter();
            m.setPlaceholderCharacter(' ');
            m.setMask("###.###.###-##");
            campo.setFormatterFactory(null);
            campo.setFormatterFactory(new DefaultFormatterFactory(m));
            campo.setValue(null);
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public static void reformatarCnpj(JFormattedTextField campo) {
        try {
            MaskFormatter m = new MaskFormatter();
            m.setPlaceholderCharacter(' ');
            m.setMask("##.###.###/####-##");
            campo.setFormatterFactory(null);
            campo.setFormatterFactory(new DefaultFormatterFactory(m));
            campo.setValue(null);
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public static void reformatarTelefone(JFormattedTextField campo) {
        try {
            MaskFormatter m = new MaskFormatter();
            m.setPlaceholderCharacter(' ');
            m.setMask("(##)####-####");
            campo.setFormatterFactory(null);
            campo.setFormatterFactory(new DefaultFormatterFactory(m));
            campo.setValue(null);
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public static String ajustaDataDMA(String data) {
        String dataFormatada = null;
        try {
            Date dataAMD = new SimpleDateFormat("yyyy-MM-dd").parse(data);
            dataFormatada = new SimpleDateFormat("dd/MM/yyyy").format(dataAMD);
        } catch (Exception e) {
            System.err.println(e);
        }
        return (dataFormatada);
    }

        public static String ajustaHora(String hora) {
        String dataFormatada = null;
        try {
            Date dataAMD = new SimpleDateFormat("mi:hh24").parse(hora);
            //dataFormatada = new SimpleDateFormat("hh24:mi").format(dataAMD);
        } catch (Exception e) {
            System.err.println(e);
        }
        return (dataFormatada);
    }

    public static String ajustaDataAMD(String data) {
        String dataFormatada = null;
        try {
            Date dataDMA = new SimpleDateFormat("dd/MM/yyyy").parse(data);
            dataFormatada = new SimpleDateFormat("yyyy-MM-dd").format(dataDMA);
        } catch (Exception e) {
            System.err.println(e);
        }
        return (dataFormatada);
    }

    public static String removerFormatacao(String dado) {
        String retorno = "";
        for (int i = 0; i < dado.length(); i++) {
            if (dado.charAt(i) != '.' && dado.charAt(i) != '/' && dado.charAt(i) != '-' && dado.charAt(i) != '(' && dado.charAt(i) != ')') {
                retorno = retorno + dado.charAt(i);
            }
        }
        return (retorno);
    }

    public static String getDataAtual() {
        Date now = new Date();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String dataHoje = df.format(now);
        
        return dataHoje;
    }

    public static String getDataHoraAtual() {
        String timeStamp; 
        timeStamp = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(Calendar.getInstance().getTime());
        System.out.println(timeStamp);
        return timeStamp;
    }
   
    public static String getHoraAtual() {
        Date now = new Date();
        DateFormat df = new SimpleDateFormat("hh24:mi");
        String dataHoje = df.format(now);
        
        return dataHoje;
    }
   
       
    public static void VinculaLostFocusCampo(JTextField campo, String nomeCampo){
        final JTextField campoValidar = campo;
        final String nomeItem = nomeCampo;
        final Color defaultColorBackground = campoValidar.getBackground();
        
        campoValidar.addFocusListener(new FocusListener() {

            @Override
            public void focusGained(FocusEvent e) {}

            @Override
            public void focusLost(FocusEvent e) {
               if(campoValidar.getText().toString().equals("")){
                    campoValidar.setToolTipText(""+nomeItem+" deve ser preenchido corretamente!");
                    campoValidar.setBackground(Color.RED);
                    //campoValidar.setBorder(new LineBorder(Color.RED));
                    campoValidar.requestFocus();
                } else{
                     campoValidar.setToolTipText(null);
                     campoValidar.setBackground(defaultColorBackground);
                     //campoValidar.setBorder(new LineBorder(Color.LIGHT_GRAY));
                 }
            }
        });
    }
    
    public static void VinculaLostFocusCampoCombo(JComboBox combo, String nomeCampo) {
        final JComboBox campoValidar = combo;
        final String nomeItem = nomeCampo;
        
        final Color defaultColorBackground = campoValidar.getBackground();
        
        campoValidar.addFocusListener(new FocusListener() {
            
            @Override
            public void focusGained(FocusEvent e) {}

            @Override
            public void focusLost(FocusEvent e) {
                
                if (campoValidar.getSelectedIndex() != -1)
                {
                    //System.out.println("OK");
                    campoValidar.setBackground(defaultColorBackground);
                }
                else
                {
                    //System.out.println("NAO SELECIONADO");
                    campoValidar.setToolTipText(""+nomeItem+" deve ser preenchido corretamente!");
                    campoValidar.setBackground(Color.RED);
                    campoValidar.requestFocus();
                }
            }
        });
    }
    public static double formataNumero(String args) throws Exception {  
        // Se você não quiser o símbolo de real  
        Locale ptBR = new Locale ("pt", "BR");  
        DecimalFormatSymbols dfs = new DecimalFormatSymbols (ptBR);  
        NumberFormat nf = new DecimalFormat ("#,##0.00", dfs);  
        double d = nf.parse (args).doubleValue(); // converte para double  
        System.out.println (nf.format (d)); // imprime "1.200,50"  
        return(d);
        // Se quiser o símbolo de real  
//        nf = NumberFormat.getCurrencyInstance (ptBR);  
//        System.out.println (nf.format (d)); // imprime "R$ 1.200,50"  
    }          
}
