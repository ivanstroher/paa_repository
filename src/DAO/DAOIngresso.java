/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import config.HibernateUtil;
import entidades.Ingressos;
import java.sql.ResultSet;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author ivan.stroher
 */
public class DAOIngresso {
    
    DAOAuditorias da = new DAOAuditorias();
    
    public Ingressos getById(int idIngresso){
        
        Ingressos i = null;
           
        List resultado = null;

        ResultSet resultadoQ;

        try {
            Session sessao = HibernateUtil.getSessionFactory().openSession();
            sessao.beginTransaction();

            // busca todos os registros
            // observar: a classe Classe no from -> C maiúsculo
            org.hibernate.Query q = sessao.createQuery("from Ingressos where idingresso = " + idIngresso);
            resultado = q.list();
            int lin = 0;
                        
            for (Object o : resultado) {
                Ingressos s = (Ingressos) o;
                i = new Ingressos();
                i.setIdingresso(s.getIdingresso());
                i.setQuantidade(s.getQuantidade());
                i.setPrecounitario(s.getPrecounitario());
                i.setIdsetor(s.getIdsetor());
                i.setIdevento(s.getIdevento());

                lin++;
               
            }
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return i;
    }

    public boolean Salvar (Ingressos i, String oper) {
        
        boolean operacao = true;
        Session sessao = null;

        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            Transaction t = sessao.beginTransaction();

            //Interfaces obj = new Interfaces();
            //obj.setNomeinterface(tfdInterface.getText());
            if("I".equals(oper)){
                sessao.save(i);
            }
            if ("D".equals(oper)){
                sessao.delete(i);
            }
            if ("U".equals(oper)){
                sessao.update(i);
            }

            t.commit();
            
        } catch (HibernateException he) {
            he.printStackTrace();
            //da.GravarLog(1,he.getCause().toString());
            da.GravarLog(da.BuscaIDinterfacePorNome("INGRESSOS"),he.getCause().toString());
            operacao = false;
        } finally {
            sessao.close();
        }
        return operacao;
    }
    
    public boolean delete(int idIngresso){
        
        Ingressos i = null;
           
        List resultado = null;

        ResultSet resultadoQ;
        boolean operacao = false;

        try {
            Session sessao = HibernateUtil.getSessionFactory().openSession();
            sessao.beginTransaction();

            // busca todos os registros
            // observar: a classe Classe no from -> C maiúsculo
            org.hibernate.Query q = sessao.createQuery("from Ingressos where idingresso = " + idIngresso);
            resultado = q.list();
            int lin = 0;

            for (Object o : resultado) {
                Ingressos s = (Ingressos) o;
                i = new Ingressos();
                
                i.setIdingresso(s.getIdingresso());
                i.setQuantidade(s.getQuantidade());
                i.setPrecounitario(s.getPrecounitario());
                i.setIdsetor(s.getIdsetor());
                i.setIdevento(s.getIdevento());

                lin++;
                
            }
            
            Salvar (i, "D");
            operacao = true;
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return operacao;
    }    
    
    public Ingressos getBySetorEvento(int idSetor, int idEvento){
        
        Ingressos i = null;
           
        List resultado = null;

        ResultSet resultadoQ;

        try {
            Session sessao = HibernateUtil.getSessionFactory().openSession();
            sessao.beginTransaction();

            // busca todos os registros
            // observar: a classe Classe no from -> C maiúsculo
            org.hibernate.Query q = sessao.createQuery("from Ingressos where idsetor = " + idSetor + " and idevento = " + idEvento);
            resultado = q.list();
            int lin = 0;
                        
            for (Object o : resultado) {
                Ingressos s = (Ingressos) o;
                i = new Ingressos();
                
                i.setIdingresso(s.getIdingresso());
                i.setQuantidade(s.getQuantidade());
                i.setPrecounitario(s.getPrecounitario());
                i.setIdsetor(s.getIdsetor());
                i.setIdevento(s.getIdevento());

                lin++;
               
            }
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return i;
    }

}
