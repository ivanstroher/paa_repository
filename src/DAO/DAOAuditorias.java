/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Controle.Formatacao;
import config.HibernateUtil;
import entidades.Auditorias;
import entidades.Interfaces;
import entidades.Log;
import entidades.Tabelaauditoria;
import interfaces.Menu;
import java.sql.ResultSet;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author ivan.stroher
 */
public class DAOAuditorias {

    public Auditorias getById(int idAuditoria){
        Auditorias i = null;
           
        List resultado = null;

        ResultSet resultadoQ;

        try {
            Session sessao = HibernateUtil.getSessionFactory().openSession();
            sessao.beginTransaction();

            // busca todos os registros
            // observar: a classe Classe no from -> C maiúsculo
            org.hibernate.Query q = sessao.createQuery("from Auditorias where idauditoria = " + idAuditoria);
            resultado = q.list();
            int lin = 0;
                        
            for (Object o : resultado) {
                Auditorias s = (Auditorias) o;
                i = new Auditorias();
                i.setIdauditoria(s.getIdauditoria());
                i.setDataauditoria(s.getDataauditoria());
                i.setTipoauditoria("INS");
                i.setIdusuarioauditoria(s.getIdusuarioauditoria());
                i.setIdtabelaauditoria(s.getIdtabelaauditoria());
                i.setValorauditoria(s.getValorauditoria());

                lin++;
               
            }
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return i;
    }

    public boolean Salvar (Auditorias i, String oper) {
        
        boolean operacao = true;
        Session sessao = null;

        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            Transaction t = sessao.beginTransaction();

            //Interfaces obj = new Interfaces();
            //obj.setNomeinterface(tfdInterface.getText());
            if("I".equals(oper)){
                sessao.save(i);
            }
            if ("D".equals(oper)){
                sessao.delete(i);
            }
            if ("U".equals(oper)){
                sessao.update(i);
            }

            t.commit();
            
        } catch (HibernateException he) {
            he.printStackTrace();
            //GravarLog(1,he.getCause().toString());
            GravarLog(BuscaIDinterfacePorNome("AUDITORIAS"),he.getCause().toString());
            operacao = false;
        } finally {
            sessao.close();
        }
        return operacao;
    }

    public boolean Salvar (Log l, String oper) {
        
        boolean operacao = true;
        Session sessao = null;

        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            Transaction t = sessao.beginTransaction();

            //Interfaces obj = new Interfaces();
            //obj.setNomeinterface(tfdInterface.getText());
            if("I".equals(oper)){
                sessao.save(l);
            }
            if ("D".equals(oper)){
                sessao.delete(l);
            }
            if ("U".equals(oper)){
                sessao.update(l);
            }

            t.commit();
            
        } catch (HibernateException he) {
            he.printStackTrace();
            //GravarLog(1,he.getMessage());
            //GravarLog(BuscaIDinterfacePorNome("LOG"),he.getCause().toString());
            operacao = false;
        } finally {
            sessao.close();
        }
        return operacao;
    }

    public boolean GravarAuditoria (int tbauditoria, String tipoAuditoria, String valorAuditoria) {
        boolean operacao = true;
            
        if (tbauditoria != 0) {

            Auditorias a = new Auditorias();

            a.setDataauditoria(Formatacao.getDataHoraAtual());
            a.setIdtabelaauditoria(tbauditoria);
            a.setIdusuarioauditoria(Menu.idUsuario);
            a.setTipoauditoria(tipoAuditoria);
            a.setValorauditoria(valorAuditoria);

            operacao = Salvar(a, "I");
        }
        
        return operacao;
    }
    
    public boolean GravarLog (int tabelaLog, String valorLog) {
        boolean operacao = true;

        Log l = new Log();
        
        l.setDatalog(Formatacao.getDataHoraAtual());
        l.setIdtabelalog(tabelaLog);
        l.setIdusuariolog(Menu.idUsuario);
        l.setValor(valorLog);

        operacao = Salvar(l, "I");
        
        return operacao;
    }
    
    public int BuscaIDinterfacePorNome(String nomeInterface) {
        int codigo = 0;
        
        List resultado = null; 

        //Interfaces i = new Interfaces();
        Tabelaauditoria i = new Tabelaauditoria();

        try {
            Session sessao = HibernateUtil.getSessionFactory().openSession();
            sessao.beginTransaction();
            
            String sql = "from Interfaces i, Tabelaauditoria t"
                       //+ " where Interfaces.nomeinterface = Tabelaauditoria.tabela"
                       + " where i.nomeinterface = t.tabela"
                       //+ " and Interfaces.nomeinterface = '"+nomeInterface.toUpperCase()+"'"
                       + " and i.nomeinterface = '"+nomeInterface.toUpperCase()+"'"
                       //+ " and Tabelaauditoria.habilitado = '"+"Sim"+"'";
                       + " and t.habilitado = '"+"Sim"+"'";
            
            org.hibernate.Query q = sessao.createQuery(sql);
            resultado = q.list();
            
            if (resultado.size() != 0) {
                sql = "from Tabelaauditoria where tabela = '"+nomeInterface.toUpperCase()+"'";

                q = sessao.createQuery(sql);
                resultado = q.list();
                
                for (Object o : resultado) {
                    i = (Tabelaauditoria) o;

                    codigo = i.getIdtabela();
                }
            }
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        
        return codigo;
    }
    
}
