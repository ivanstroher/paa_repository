/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import config.HibernateUtil;
import entidades.Tabelaauditoria;
import java.sql.ResultSet;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author ivan.stroher
 */
public class DAOTabelaAuditoria {
    
    DAOAuditorias da = new DAOAuditorias();
    
    public Tabelaauditoria getById(int idTabela){
        
        Tabelaauditoria i = null;
        //Interfaces ip = null;
           
        List resultado = null;

        ResultSet resultadoQ;

        try {
            Session sessao = HibernateUtil.getSessionFactory().openSession();
            sessao.beginTransaction();

            // busca todos os registros
            // observar: a classe Classe no from -> C maiúsculo
            org.hibernate.Query q = sessao.createQuery("from Tabelaauditoria where idTabela = " + idTabela );
            resultado = q.list();
            int lin = 0;
                        
            for (Object o : resultado) {
                Tabelaauditoria s = (Tabelaauditoria) o;
                i = new Tabelaauditoria();
                i.setIdtabela(s.getIdtabela());
                i.setTabela(s.getTabela());
                i.setHabilitado(s.getHabilitado());

                lin++;
               
            }
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return i;
    }

    public boolean Salvar (Tabelaauditoria i, String oper) {
        
        boolean operacao = true;
        Session sessao = null;

        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            Transaction t = sessao.beginTransaction();

            //Interfaces obj = new Interfaces();
            //obj.setNomeinterface(tfdInterface.getText());
            if("I".equals(oper)){
                sessao.save(i);
            }
            if ("D".equals(oper)){
                    sessao.delete(i);
            }
            if ("U".equals(oper)){
                sessao.update(i);
            }

            t.commit();
            
        } catch (HibernateException he) {
            he.printStackTrace();
            da.GravarLog(1,he.getCause().toString());
            operacao = false;
        } finally {
            sessao.close();
        }
        return operacao;
    }
    
    public boolean delete(int idTabela){
        
        Tabelaauditoria i = null;
           
        List resultado = null;

        ResultSet resultadoQ;
        boolean operacao = false;

        try {
            Session sessao = HibernateUtil.getSessionFactory().openSession();
            sessao.beginTransaction();

            // busca todos os registros
            // observar: a classe Classe no from -> C maiúsculo
            org.hibernate.Query q = sessao.createQuery("from Tabelaauditoria where idtabela = " + idTabela);
            resultado = q.list();
            int lin = 0;

            for (Object o : resultado) {
                Tabelaauditoria s = (Tabelaauditoria) o;
                i = new Tabelaauditoria();
                i.setIdtabela(s.getIdtabela());
                i.setTabela(s.getTabela());
                i.setHabilitado(s.getHabilitado());

                lin++;
                
            }
            
            Salvar (i, "D");
            operacao = true;
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return operacao;
    }    
    
    public Tabelaauditoria getTabela(String search){
        
        Tabelaauditoria i = null;
           
        List resultado = null;

        ResultSet resultadoQ;

        try {
            Session sessao = HibernateUtil.getSessionFactory().openSession();
            sessao.beginTransaction();

            // busca todos os registros
            // observar: a classe Classe no from -> C maiúsculo
            org.hibernate.Query q = sessao.createQuery("from Tabelaauditoria WHERE tabela LIKE '%" + search + "%' order by idtabela");
            resultado = q.list();
            int lin = 0;
                        
            for (Object o : resultado) {
                Tabelaauditoria s = (Tabelaauditoria) o;
                i = new Tabelaauditoria();
                i.setIdtabela(s.getIdtabela());
                i.setTabela(s.getTabela());
                i.setHabilitado(s.getHabilitado());

                lin++;
               
            }
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return i;
    }  
    
    private boolean getAudita(String tabela){
        
        Tabelaauditoria i = null;
           
        List resultado = null;

        ResultSet resultadoQ;
        boolean operacao = false;

        try {
            Session sessao = HibernateUtil.getSessionFactory().openSession();
            sessao.beginTransaction();

            // busca todos os registros
            // observar: a classe Classe no from -> C maiúsculo
            org.hibernate.Query q = sessao.createQuery("from Tabelaauditoria where tabela = '" + tabela + "' and habilitado = 'Sim'" );
            resultado = q.list();
            int lin = 0;
                        
            for (Object o : resultado) {
                Tabelaauditoria s = (Tabelaauditoria) o;
                i = new Tabelaauditoria();
                i.setIdtabela(s.getIdtabela());
                i.setTabela(s.getTabela());
                i.setHabilitado(s.getHabilitado());
                operacao = true;

                lin++;
               
            }
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return operacao;
    }
    public Tabelaauditoria BuscaIDTabelaPorNome(String nomeTabela) {
        
    
        Tabelaauditoria i = null;
        //Interfaces ip = null;
           
        List resultado = null;

        ResultSet resultadoQ;

        try {
            Session sessao = HibernateUtil.getSessionFactory().openSession();
            sessao.beginTransaction();

            // busca todos os registros
            // observar: a classe Classe no from -> C maiúsculo
            org.hibernate.Query q = sessao.createQuery("from Tabelaauditoria where tabela = " + nomeTabela );
            resultado = q.list();
            int lin = 0;
                        
            for (Object o : resultado) {
                Tabelaauditoria s = (Tabelaauditoria) o;
                i = new Tabelaauditoria();
                i.setIdtabela(s.getIdtabela());
                i.setTabela(s.getTabela());
                i.setHabilitado(s.getHabilitado());

                lin++;
               
            }
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return i;  
    }
    
}
