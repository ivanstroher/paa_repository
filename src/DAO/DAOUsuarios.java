/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import config.HibernateUtil;
import entidades.Grupos;
import entidades.Usuarios;
import java.sql.ResultSet;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author ivan.stroher
 */
public class DAOUsuarios {
    
    DAOAuditorias da = new DAOAuditorias();
    
    public Usuarios getById(int idUsuario){
        
        Usuarios i = null;
           
        List resultado = null;

        ResultSet resultadoQ;

        try {
            Session sessao = HibernateUtil.getSessionFactory().openSession();
            sessao.beginTransaction();

            // busca todos os registros
            // observar: a classe Classe no from -> C maiúsculo
            org.hibernate.Query q = sessao.createQuery("from Usuarios where idUsuario = " + idUsuario);
            resultado = q.list();
            int lin = 0;
                        
            for (Object o : resultado) {
                Usuarios s = (Usuarios) o;
                i = new Usuarios();
                
                i.setIdusuario(s.getIdusuario());
                i.setNome(s.getNome());
                i.setLogin(s.getLogin());
                i.setSenha(s.getSenha());
                i.setStatus(s.getStatus());
                i.setIdgrupo(s.getIdgrupo());

                lin++;
               
            }
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return i;
    }

    public boolean Salvar (Usuarios i, String oper) {
        
        boolean operacao = true;
        Session sessao = null;

        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            Transaction t = sessao.beginTransaction();

            //Interfaces obj = new Interfaces();
            //obj.setNomeinterface(tfdInterface.getText());
            if("I".equals(oper)){
                sessao.save(i);
            }
            if ("D".equals(oper)){
                sessao.delete(i);
            }
            if ("U".equals(oper)){
                sessao.update(i);
            }

            t.commit();
            
        } catch (HibernateException he) {
            he.printStackTrace();
            //da.GravarLog(1,he.getCause().toString());
            da.GravarLog(da.BuscaIDinterfacePorNome("USUARIOS"),he.getCause().toString());
            operacao = false;
        } finally {
            sessao.close();
            
        }
        return operacao;
    }
    
    public boolean delete(int idUsuario){
        
        Usuarios i = null;
           
        List resultado = null;

        ResultSet resultadoQ;
        boolean operacao = false;

        try {
            Session sessao = HibernateUtil.getSessionFactory().openSession();
            sessao.beginTransaction();

            // busca todos os registros
            // observar: a classe Classe no from -> C maiúsculo
            org.hibernate.Query q = sessao.createQuery("from Usuarios where idUsuario = " + idUsuario);
            resultado = q.list();
            int lin = 0;

            for (Object o : resultado) {
                Usuarios s = (Usuarios) o;
                i = new Usuarios();
                
                i.setIdusuario(s.getIdusuario());
                i.setNome(s.getNome());
                i.setLogin(s.getLogin());
                i.setSenha(s.getSenha());
                i.setStatus(s.getStatus());
//                if(i.getStatus()=='I'){
//                    i.setStatus('I');
//                } else {
//                    i.setStatus('A');
//                }
                i.setIdgrupo(s.getIdgrupo());

                lin++;
                
            }
            
            Salvar (i, "D");
            operacao = true;
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return operacao;
    }    
    
  
}
