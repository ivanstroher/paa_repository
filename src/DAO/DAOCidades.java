/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import config.HibernateUtil;
import entidades.Cidades;
import java.sql.ResultSet;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author ivan.stroher
 */
public class DAOCidades {
    
    DAOAuditorias da = new DAOAuditorias();
    
    public Cidades getById(int idCidade){
        Cidades i = null;
           
        List resultado = null;

        ResultSet resultadoQ;

        try {
            Session sessao = HibernateUtil.getSessionFactory().openSession();
            sessao.beginTransaction();

            // busca todos os registros
            // observar: a classe Classe no from -> C maiúsculo
            org.hibernate.Query q = sessao.createQuery("from Cidades where idcidade = " + idCidade);
            resultado = q.list();
            int lin = 0;
                        
            for (Object o : resultado) {
                Cidades s = (Cidades) o;
                i = new Cidades();
                i.setIdcidade(s.getIdcidade());
                i.setNomecidade(s.getNomecidade());
                i.setUf(s.getUf());
                i.setCep(s.getCep());

                lin++;
               
            }
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return i;
    }

    public boolean Salvar (Cidades i, String oper) {
        
        boolean operacao = true;
        Session sessao = null;

        try {
            sessao = HibernateUtil.getSessionFactory().openSession();
            Transaction t = sessao.beginTransaction();

            if("I".equals(oper)){
                sessao.save(i);
            }
            if ("D".equals(oper)){
                    sessao.delete(i);
            }
            if ("U".equals(oper)){
                sessao.update(i);
            }

            t.commit();
            
        } catch (HibernateException he) {
            he.printStackTrace();
            //da.GravarLog(1,he.getCause().toString());
            da.GravarLog(da.BuscaIDinterfacePorNome("CIDADES"),he.getCause().toString());
            operacao = false;
        } finally {
            sessao.close();
        }
        return operacao;
    }
    public boolean delete(int idCidade){
        
        Cidades i = null;
           
        List resultado = null;

        ResultSet resultadoQ;
        boolean operacao = false;

        try {
            Session sessao = HibernateUtil.getSessionFactory().openSession();
            sessao.beginTransaction();

            // busca todos os registros
            // observar: a classe Classe no from -> C maiúsculo
            org.hibernate.Query q = sessao.createQuery("from Cidades where idcidade = " + idCidade);
            resultado = q.list();
            int lin = 0;

            for (Object o : resultado) {
                Cidades s = (Cidades) o;
                i = new Cidades();
                
                i.setIdcidade(s.getIdcidade());
                i.setNomecidade(s.getNomecidade());
                i.setUf(s.getUf());
                i.setCep(s.getCep());

                lin++;
                
            }
            
            Salvar (i, "D");
            operacao = true;
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        return operacao;
    }    
        
}
