/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import Controle.Formatacao;
import Controle.MaskMonetario;
import DAO.DAOIngresso;
import DAO.DAOEventos;
import DAO.DAOSetores;
import entidades.Eventos;
import entidades.Ingressos;
import entidades.Setores;
import Controle.Util;
import DAO.DAOAuditorias;
import config.HibernateUtil;
import java.sql.ResultSet;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author Lazaro
 */
public class FrmCadastroIngresso extends javax.swing.JDialog {

    private Eventos eventos;
    private Ingressos ingressos;
    private Setores setores;
    
    java.awt.Frame parent;
    
    private Ingressos ingressoAtual = null;
    
    DAOAuditorias da = new DAOAuditorias();
    String valorAuditoria = "";
    String IncAlt = "ALT";
    
    /**
     * Creates new form FrmCadastroIngresso
     */
    
    //public FrmCadastroIngresso(java.awt.Frame parent, boolean modal) {
    //    super(parent, modal);
    //    initComponents();
    //}
    public FrmCadastroIngresso(java.awt.Frame parent, boolean modal, int idIngresso) {
        super(parent, modal);
        initComponents();
        this.parent = parent;
        //Formatacao.alinhaADireita(tfdValor);
        //this.tfdValor.setDocument(new MaskMonetario());
        // Se recebeu parametro de algum produto para edição
        // faz consulta pelo id da grupo

        if(idIngresso>0){
            
            DAOIngresso dao= new DAOIngresso();
            this.ingressoAtual = dao.getById(idIngresso); 
            
        }        
        // seta os campos no formulario
        this.setaCampos();
    }

    private void setaCampos(){
        if(this.ingressoAtual!=null){        
            valorAuditoria = "Código: "+this.ingressoAtual.getIdingresso()+" | ";

            tfdEvento.setText(String.valueOf(this.ingressoAtual.getIdevento().getIdevento()));
            valorAuditoria = valorAuditoria + "ID Evento: "+tfdEvento.getText()+" | ";
            
            tfdNomeEvento.setText(String.valueOf(this.ingressoAtual.getIdevento().getDescricao()));
            valorAuditoria = valorAuditoria + "Nome Evento: "+tfdNomeEvento.getText()+" | ";
            
            tfdSetor.setText(String.valueOf(this.ingressoAtual.getIdsetor().getIdsetor()));
            valorAuditoria = valorAuditoria + "ID Setor: "+tfdSetor.getText()+" | ";
            
            tfdNomeSetor.setText(String.valueOf(this.ingressoAtual.getIdsetor().getDescricao()));
            valorAuditoria = valorAuditoria + "Nome Setor: "+tfdNomeSetor.getText()+" | ";
            
            tfdValor.setText(String.valueOf(this.ingressoAtual.getPrecounitario()));
            valorAuditoria = valorAuditoria + "Preço Unitário: "+tfdValor.getText()+" | ";
            
 
        }  
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnFechar = new javax.swing.JButton();
        tfdEvento = new javax.swing.JTextField();
        tfdNomeEvento = new javax.swing.JTextField();
        tfdNomeSetor = new javax.swing.JTextField();
        tfdValor = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        tfdSetor = new javax.swing.JTextField();
        btnSalvar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        btnFechar.setText("Fechar");
        btnFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFecharActionPerformed(evt);
            }
        });

        tfdEvento.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfdEventoFocusLost(evt);
            }
        });

        tfdNomeEvento.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfdNomeEventoFocusLost(evt);
            }
        });

        tfdNomeSetor.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfdNomeSetorFocusLost(evt);
            }
        });

        tfdValor.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfdValorFocusLost(evt);
            }
        });

        jLabel2.setText("Evento");

        jPanel1.setBackground(new java.awt.Color(51, 255, 255));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        jLabel1.setText("Ingresso");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(292, 292, 292)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1)
                .addContainerGap(22, Short.MAX_VALUE))
        );

        jLabel4.setText("Valor");

        jLabel5.setText("Setor");

        tfdSetor.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfdSetorFocusLost(evt);
            }
        });

        btnSalvar.setText("Salvar");
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(tfdEvento, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(tfdNomeEvento, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel4))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(tfdSetor, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(tfdNomeSetor, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(tfdValor, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(404, 404, 404))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(tfdEvento, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfdNomeEvento, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfdSetor, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(tfdNomeSetor, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfdValor, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 178, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnFechar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFecharActionPerformed
        dispose();
    }//GEN-LAST:event_btnFecharActionPerformed

    private void tfdEventoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfdEventoFocusLost
        if(this.tfdEvento.getText().equals("")){
            this.tfdEvento.setText("");
        }  else {
            DAOEventos dao = new DAOEventos();
            eventos = dao.getById(Integer.parseInt(this.tfdEvento.getText()));

            if (eventos == null) {
                Util.Aviso("Evento inexistente!");
                this.tfdEvento.requestFocus();
            } else {
                this.tfdNomeEvento.setText(eventos.getDescricao());
                this.tfdNomeEvento.disable();
            }
        }
    }//GEN-LAST:event_tfdEventoFocusLost

    private void tfdNomeEventoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfdNomeEventoFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_tfdNomeEventoFocusLost

    private void tfdNomeSetorFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfdNomeSetorFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_tfdNomeSetorFocusLost

    private void tfdValorFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfdValorFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_tfdValorFocusLost

    private void tfdSetorFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfdSetorFocusLost
        if(this.tfdSetor.getText().equals("")){
            this.tfdSetor.setText("");
        }  else {
            DAOSetores dao = new DAOSetores();
            setores = dao.getById(Integer.parseInt(this.tfdSetor.getText()));

            if (setores == null) {
                Util.Aviso("Setor inexistente!");
                this.tfdSetor.requestFocus();
            } else {
                this.tfdNomeSetor.setText(setores.getDescricao());
                this.tfdNomeSetor.disable();
            }
        }
    }//GEN-LAST:event_tfdSetorFocusLost

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        if(tfdEvento.getText().equals("")){
            Util.Aviso("Informe o evento!");
            tfdEvento.requestFocus();
        }else if(tfdSetor.getText().trim().equals("") ){
            Util.Aviso("Informe o setor!");
            tfdSetor.requestFocus();
        }else if(tfdValor.getText().trim().equals("") ){
            Util.Aviso("Informe o valor!");
            tfdValor.requestFocus();
        } else {
            if(ingressoAtual==null)
            ingressoAtual = new Ingressos();

            ingressoAtual.setIdevento(eventos);
            ingressoAtual.setIdsetor(setores);
            ingressoAtual.setPrecounitario(Double.parseDouble(tfdValor.getText()));

            boolean operacao = false;

            DAOIngresso dao = new DAOIngresso();

            if(this.ingressoAtual.getIdingresso()== null) {
                operacao = dao.Salvar(ingressoAtual, "I");
                IncAlt = "INS";
            } else
            operacao = dao.Salvar(ingressoAtual, "U");

            if(!operacao)
            {
                Util.Aviso("Erro ao gravar procedimento!");
                dispose();
            }else{
                if (IncAlt.equals("INS")) {
                    valorAuditoria = "Código: "+ingressoAtual.getIdingresso()+" | "+
                                     "Código Evento: "+tfdEvento.getText()+" | "+
                                     "Evento: "+tfdNomeEvento.getText()+" | "+
                                     "Código Setor: "+tfdSetor.getText()+" | "+
                                     "Setor: "+tfdNomeSetor.getText()+" | "+
                                     "Valor: "+tfdValor.getText();
                }

                da.GravarAuditoria(da.BuscaIDinterfacePorNome("INGRESSOS"),IncAlt,valorAuditoria);

                Util.Aviso("Dados gravados com sucesso!!");
                dispose();
            }
        }
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void Buscar (int idIngresso) {
        List resultado = null;

        ResultSet resultadoQ;

        // dados da tabela
        Object[][] dadosTabela = null;

        // cabecalho da tabela
        Object[] cabecalho = new Object[5];
        cabecalho[0] = "Código";
        cabecalho[1] = "Quantidade";
        cabecalho[2] = "Preço Unitário";
        cabecalho[3] = "Setor";
        cabecalho[4] = "Evento";

        try {
            Session sessao = HibernateUtil.getSessionFactory().openSession();
            sessao.beginTransaction();

            // busca por código
//            int id = 3;
//            org.hibernate.Query q = sessao.createQuery("from Classe where id = " + id);

            // busca todos os registros
            // observar: a classe Classe no from -> C maiúsculo
            org.hibernate.Query q = sessao.createQuery("from Ingressos where idingresso = " + idIngresso);
            resultado = q.list();
            int lin = 0;

            dadosTabela = new Object[q.list().size()][5];
            
            for (Object o : resultado) {
                Ingressos s = (Ingressos) o;
                
                dadosTabela[lin][0] = s.getIdingresso();
                dadosTabela[lin][1] = s.getQuantidade();
                dadosTabela[lin][2] = s.getPrecounitario();
                dadosTabela[lin][3] = s.getIdsetor();
                dadosTabela[lin][4] = s.getIdevento();

                lin++;
                
                //dadosTabela = new Object[lin][3];
                
                //System.out.print("id: " + s.getId() + " ");
                //System.out.println("nome: " + s.getNome());
                //this.interfaceAtual.setIdinterface(s.getIdinterface());
                //this.interfaceAtual.setNomeinterface(s.getNomeinterface());
                
                tfdValor.setText(String.valueOf(s.getPrecounitario()));
                tfdEvento.setText(String.valueOf(s.getIdevento()));
                tfdNomeEvento.setText(s.getIdevento().getDescricao());
                tfdSetor.setText(String.valueOf(s.getIdsetor()));
                tfdNomeSetor.setText(s.getIdsetor().getDescricao());
            }
        } catch (HibernateException he) {
            he.printStackTrace();
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnFechar;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField tfdEvento;
    private javax.swing.JTextField tfdNomeEvento;
    private javax.swing.JTextField tfdNomeSetor;
    private javax.swing.JTextField tfdSetor;
    private javax.swing.JTextField tfdValor;
    // End of variables declaration//GEN-END:variables
}
