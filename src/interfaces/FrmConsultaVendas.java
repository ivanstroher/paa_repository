/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import Controle.Validacao;
import Controle.Formatacao;
import Controle.Util;
import Tabelas.VwvendasTbl;
import config.HibernateUtil;
import entidades.Vwvendas;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;
import org.hibernate.HibernateException;
import org.hibernate.Session;
/**
 *
 * @author ivan.stroher
 */
public class FrmConsultaVendas extends javax.swing.JDialog {

    private java.awt.Frame parent;
    
    private VwvendasTbl modelVenda;
    
    private List<Vwvendas> listaVenda;
    /**
     * Creates new form FrmConsultaVendas
     */
    public FrmConsultaVendas(java.awt.Frame parent, boolean modal, String menu) {
        super(parent, modal);
        initComponents();
        rbDataVenda.setSelected(true);
        jVendas.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        
         /* AÇÃO DE PRESSIONAR ESC */
        btnFechar.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ESCAPE"), "btnFecharActionPerformed");
        btnFechar.getActionMap().put("btnFecharActionPerformed",
            new AbstractAction("btnFecharActionPerformed")
            {
                @Override
                public void actionPerformed(ActionEvent evt) {
                    dispose();
                }
            }
        );        
           
        jVendas.addMouseListener(new MouseAdapter(){
            
        });        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        tfdDataf = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        tfdEvento = new javax.swing.JTextField();
        tfdDatai = new javax.swing.JTextField();
        btnBuscar = new javax.swing.JButton();
        tfdSetor = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jVendas = new javax.swing.JTable();
        btnFechar = new javax.swing.JButton();
        rbDataEvento = new javax.swing.JRadioButton();
        rbDataVenda = new javax.swing.JRadioButton();

        jPanel1.setBackground(new java.awt.Color(51, 255, 255));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Consulta Vendas");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 828, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tfdDataf = Formatacao.getData();
        tfdDataf.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfdDatafFocusLost(evt);
            }
        });

        jLabel3.setText("Período");

        jLabel2.setText("Evento");

        tfdDatai = Formatacao.getData();
        tfdDatai.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfdDataiActionPerformed(evt);
            }
        });
        tfdDatai.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfdDataiFocusLost(evt);
            }
        });

        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        jLabel4.setText("Setor");

        jVendas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jVendas);

        btnFechar.setText("Fechar");
        btnFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFecharActionPerformed(evt);
            }
        });

        rbDataEvento.setText("Data evento");
        rbDataEvento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbDataEventoActionPerformed(evt);
            }
        });

        rbDataVenda.setText("Data venda");
        rbDataVenda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbDataVendaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tfdEvento, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(tfdSetor, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(tfdDatai, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(rbDataEvento))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(rbDataVenda)
                                    .addComponent(tfdDataf, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 98, Short.MAX_VALUE)
                        .addComponent(btnBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(97, 97, 97)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnBuscar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(tfdDatai)
                    .addComponent(tfdSetor, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE)
                    .addComponent(tfdEvento, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE)
                    .addComponent(tfdDataf))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rbDataEvento)
                    .addComponent(rbDataVenda))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 357, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 514, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tfdDataiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfdDataiActionPerformed
        if (tfdDatai.getText().equals("")){
        } else {
            Validacao.validarDataFormatada(tfdDatai.getText());
        }
    }//GEN-LAST:event_tfdDataiActionPerformed

    private void tfdDataiFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfdDataiFocusLost
        if (tfdDatai.getText().equals("")){
            Util.Aviso("Informe a data inicial do período!!!");
            tfdDatai.requestFocus();
        }
        if (Validacao.validarDataFormatada(tfdDatai.getText())){
        } else {
            Util.Aviso("Data início inválida, verifique!!!");
            tfdDatai.requestFocus();
        }
    }//GEN-LAST:event_tfdDataiFocusLost

    private void tfdDatafFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfdDatafFocusLost
        if (tfdDataf.getText().equals("")){
            Util.Aviso("Informe a data final do período!!!");
            tfdDataf.requestFocus();
        }        
        if (Validacao.validarDataFormatada(tfdDataf.getText())){
        } else {
            Util.Aviso("Data final inválida, verifique!!!");
            tfdDataf.requestFocus();
        }
    }//GEN-LAST:event_tfdDatafFocusLost

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        List resultado = null;

        ResultSet resultadoQ;

        // dados da tabela
        Object[][] dadosTabela = null;

        // cabecalho da tabela
        Object[] cabecalho = new Object[8];
        cabecalho[0] = "Evento";
        cabecalho[1] = "Data";
        cabecalho[2] = "Hora";
        cabecalho[3] = "Local";
        cabecalho[4] = "Setor";
        cabecalho[5] = "Dt venda";
        cabecalho[6] = "Qtde";
        cabecalho[7] = "Valor";

        String query = null;

        String pdatai = null;
        String pdataf = null;
        String vdataEvento = null;
        String vdataVenda = null;
        
        query = "from Vwvendas where 1=1 ";

        try {
            Session sessao = HibernateUtil.getSessionFactory().openSession();
            sessao.beginTransaction();

            // busca todos os registros
            // observar: a classe Classe no from -> C maiúsculo
            //org.hibernate.Query q = sessao.createQuery("from Auditorias a, Tabelaauditoria t where a.tabelaauditoria = t.idtabela and t.tabela like '%" + tfdTabela.getText() + "%' order by a.dataauditoria desc");
            
            if (Formatacao.removerFormatacao(tfdDatai.getText()).trim().equals("")){
                Util.Aviso("Informe qual período de data deseja consultar!!!");
                tfdDatai.requestFocus();
            } else {
                pdatai = tfdDatai.getText();
                pdataf = tfdDataf.getText();
                if (rbDataVenda.isSelected()){
//                    query = query + " and datavenda between '" + pdatai + " 00:00' and '" + pdataf + " 23:59'";
                    query = query + " and datavenda between '" + Formatacao.ajustaDataAMD(pdatai) + "' and '" + Formatacao.ajustaDataAMD(pdataf) + "'";
                } else {
//                    query = query + " and dataevento between '" + pdatai + " 00:00' and '" + pdataf + " 23:59'";
                    query = query + " and dataevento between '" + Formatacao.ajustaDataAMD(pdatai) + "' and '" + Formatacao.ajustaDataAMD(pdataf) + "'";
                }
            //}

                if (tfdEvento.getText().trim().equals("")){
                } else {
                    query = query + " and descricao like '%" + tfdEvento.getText() + "%'";
                }

                if (tfdSetor.getText().trim().equals("")){
                } else {
                    query = query + " and setor like '%" + tfdSetor.getText() + "%'";
                }

                org.hibernate.Query q = sessao.createQuery(query);
                resultado = q.list();

                int lin = 0;

                dadosTabela = new Object[q.list().size()][8];

                for (Object o : resultado) {
                    Vwvendas s = (Vwvendas) o;
                    
                    vdataEvento = Formatacao.ajustaDataDMA(s.getDataevento());
                    vdataVenda = Formatacao.ajustaDataDMA(s.getDatavenda().toString());
                    dadosTabela[lin][0] = s.getDescricao();
                    dadosTabela[lin][1] = vdataEvento; //s.getDataevento();
                    dadosTabela[lin][2] = s.getHoraevento();
                    dadosTabela[lin][3] = s.getLocal();
                    dadosTabela[lin][4] = s.getSetor();
                    dadosTabela[lin][5] = vdataVenda;//s.getDatavenda();
                    dadosTabela[lin][6] = s.getVendidos();
                    dadosTabela[lin][7] = s.getValortotal();

                    lin++;

                }
                modelVenda = new VwvendasTbl((ArrayList<Vwvendas>) resultado);
                jVendas.isCellEditable(0,0);
                jVendas.setModel(new DefaultTableModel(dadosTabela, cabecalho));
                jScrollPane1.setViewportView(jVendas);
            }
        } catch (HibernateException he) {
            he.printStackTrace();
        }
        
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void btnFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFecharActionPerformed
        dispose();
    }//GEN-LAST:event_btnFecharActionPerformed

    private void rbDataEventoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbDataEventoActionPerformed
        if (rbDataEvento.isSelected()) {
            rbDataVenda.setSelected(false);
        } 
    }//GEN-LAST:event_rbDataEventoActionPerformed

    private void rbDataVendaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbDataVendaActionPerformed
        if (rbDataVenda.isSelected()) {
            rbDataEvento.setSelected(false);
        } 
    }//GEN-LAST:event_rbDataVendaActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmConsultaVendas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmConsultaVendas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmConsultaVendas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmConsultaVendas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            private String menu;
            @Override
            public void run() {
                FrmConsultaVendas dialog = new FrmConsultaVendas(new javax.swing.JFrame(), true, menu);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnFechar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jVendas;
    private javax.swing.JRadioButton rbDataEvento;
    private javax.swing.JRadioButton rbDataVenda;
    private javax.swing.JTextField tfdDataf;
    private javax.swing.JTextField tfdDatai;
    private javax.swing.JTextField tfdEvento;
    private javax.swing.JTextField tfdSetor;
    // End of variables declaration//GEN-END:variables
}
