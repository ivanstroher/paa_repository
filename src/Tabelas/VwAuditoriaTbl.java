/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tabelas;

import entidades.Vwauditorias;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author ivan.stroher
 */
public class VwAuditoriaTbl extends AbstractTableModel{
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
    private final int COL_USU = 0;
    private final int COL_LOG = 1;
    private final int COL_DAT = 2;
    private final int COL_TIP = 3;
    private final int COL_TAB = 4;
    private final int COL_VLR = 5;
    
    public List<Vwauditorias> listaAuditoria;

    public VwAuditoriaTbl(ArrayList<Vwauditorias> lista) {
        listaAuditoria = lista;
    }

    @Override
    public int getRowCount() {
        return listaAuditoria.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public String getColumnName(int column) {
        
        if (column == COL_USU) {
            return "Usuário";
        } else if (column == COL_LOG) {
            return "Login";            
        } else if (column == COL_DAT) {
            return "Data";
        } else if (column == COL_TIP) {
            return "Tipo";
        } else if (column == COL_TAB) {
            return "Tabela";  
        } else if (column == COL_VLR) {
            return "Valor";            
        }
        return "";
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == COL_USU) {
            return String.class;
        } else if (columnIndex == COL_LOG) {
            return String.class;            
        } else if (columnIndex == COL_DAT) {
            return Date.class;
        } else if (columnIndex == COL_TIP) {
            return String.class;
        } else if (columnIndex == COL_TAB) {
            return String.class;
        } else if (columnIndex == COL_VLR) {
            return String.class;            
        } 
        return String.class;
    }
    public int getCodigo(int rowIndex){
        Vwauditorias p = listaAuditoria.get(rowIndex);
        return p.getIdauditoria();
    }
    public Object getValueAt(int rowIndex, int columnIndex) {
        Vwauditorias p = listaAuditoria.get(rowIndex);
        //verifica qual valor deve ser retornado
        if (columnIndex == COL_USU) {
            return p.getIdauditoria();
        } else if (columnIndex == COL_LOG) {
            return p.getLogin();            
        } else if (columnIndex == COL_DAT) {
            return p.getDataauditoria();
        } else if (columnIndex == COL_TIP) {
            return p.getTipoauditoria();
        } else if (columnIndex == COL_TAB) {
            return p.getTabela();
        } else if (columnIndex == COL_VLR) {
            return p.getValorauditoria();            
        } else return "";
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public Vwauditorias getAuditoria(int pos) {
        if (pos >= listaAuditoria.size())
        {    
            return null;
        } else {
            return listaAuditoria.get(pos);
        }
    }       
}
