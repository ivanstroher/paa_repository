/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tabelas;

import entidades.Eventos;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author ivan.stroher
 */
public class EventoTbl extends AbstractTableModel{
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
    private final int COL_COD = 0;
    private final int COL_DSC = 1;
    private final int COL_DT = 2;
    private final int COL_HR = 3;
    private final int COL_TI = 4;
    private final int COL_FRN = 5;
    private final int COL_LOC = 6;
    
    public List<Eventos> listaEventos;

    public EventoTbl(ArrayList<Eventos> lista) {
        listaEventos = lista;
    }

    @Override
    public int getRowCount() {
        return listaEventos.size();
    }

    @Override
    public int getColumnCount() {
        return 7;
    }

    @Override
    public String getColumnName(int column) {
        
        if (column == COL_COD) {
            return "Código";
        } else if (column == COL_DSC) {
            return "Descrição";
        } else if (column == COL_DT) {
            return "Data";     
        } else if (column == COL_HR) {
            return "Hora";  
        } else if (column == COL_TI) {
            return "Total ingressos";  
        } else if (column == COL_FRN) {
            return "Fornecedor";  
        } else if (column == COL_LOC) {
            return "Local";              
        }
        return "";
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == COL_COD) {
            return String.class;
        } else if (columnIndex == COL_DSC) {
            return String.class;
        } else if (columnIndex == COL_LOC) {
            return String.class; 
        } else if (columnIndex == COL_DT) {
            return String.class; 
        } else if (columnIndex == COL_HR) {
            return String.class;     
        } else if (columnIndex == COL_TI) {
            return String.class;     
        } else if (columnIndex == COL_FRN) {
            return String.class;  
        } else if (columnIndex == COL_LOC) {
            return String.class;                 
        } 
        return String.class;
    }
    public int getCodigo(int rowIndex){
        Eventos p = listaEventos.get(rowIndex);
        return p.getIdevento();
    }
    public Object getValueAt(int rowIndex, int columnIndex) {
        Eventos p = listaEventos.get(rowIndex);
        //verifica qual valor deve ser retornado
        if (columnIndex == COL_COD) {
            return p.getIdlocal();
        } else if (columnIndex == COL_DSC) {
            return p.getDescricao();
        } else if (columnIndex == COL_DT) {
            return p.getDataevento();  
        } else if (columnIndex == COL_HR) {
            return p.getHoraevento();  
        } else if (columnIndex == COL_TI) {
            return p.getTotalingressos();     
        } else if (columnIndex == COL_FRN) {
            return p.getIdfornecedor().getNome();     
        } else if (columnIndex == COL_LOC) {
            return p.getIdlocal().getDescricao();                 
        } else return "";
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public Eventos getEvento(int pos) {
        if (pos >= listaEventos.size())
        {    
            return null;
        } else {
            return listaEventos.get(pos);
        }
    }       
}
