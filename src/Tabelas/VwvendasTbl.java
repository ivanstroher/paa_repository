/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tabelas;

import entidades.Vwlogs;
import entidades.Vwvendas;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author ivan.stroher
 */
public class VwvendasTbl extends AbstractTableModel{
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
    private final int COL_EVE = 0;
    private final int COL_DT = 1;
    private final int COL_HR = 2;
    private final int COL_LOC = 3;
    private final int COL_SET = 4;
    private final int COL_DTV = 5;
    private final int COL_QTD = 6;
    private final int COL_VLR = 7;
    
    public List<Vwvendas> listaVenda;

    public VwvendasTbl(ArrayList<Vwvendas> lista) {
        listaVenda = lista;
    }

    @Override
    public int getRowCount() {
        return listaVenda.size();
    }

    @Override
    public int getColumnCount() {
        return 7;
    }

    @Override
    public String getColumnName(int column) {
        
        if (column == COL_EVE) {
            return "Evento";
        } else if (column == COL_DT) {
            return "Dt Evento";            
        } else if (column == COL_HR) {
            return "Hora";
        } else if (column == COL_LOC) {
            return "Local";  
        } else if (column == COL_SET) {
            return "Setor";   
        } else if (column == COL_DTV) {
            return "Data venda";  
        } else if (column == COL_QTD) {
            return "Qtde";  
        } else if (column == COL_VLR) {
            return "Valor";              
        }
        return "";
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == COL_EVE) {
            return String.class;
        } else if (columnIndex == COL_DT) {
            return Date.class;            
        } else if (columnIndex == COL_HR) {
            return String.class;
        } else if (columnIndex == COL_LOC) {
            return String.class;
        } else if (columnIndex == COL_SET) {
            return String.class;
        } else if (columnIndex == COL_DTV) {
            return Date.class;    
        } else if (columnIndex == COL_QTD) {
            return Integer.class;    
        } else if (columnIndex == COL_VLR) {
            return Double.class;                
        } 
        return String.class;
    }
    public int getCodigo(int rowIndex){
        Vwvendas p = listaVenda.get(rowIndex);
        return p.getIdevento();
    }
    public Object getValueAt(int rowIndex, int columnIndex) {
        Vwvendas p = listaVenda.get(rowIndex);
        //verifica qual valor deve ser retornado
        if (columnIndex == COL_EVE) {
            return p.getDescricao();
        } else if (columnIndex == COL_DT) {
            return p.getDataevento();            
        } else if (columnIndex == COL_HR) {
            return p.getHoraevento();
        } else if (columnIndex == COL_LOC) {
            return p.getLocal();
        } else if (columnIndex == COL_SET) {
            return p.getSetor();   
        } else if (columnIndex == COL_DTV) {
            return p.getDatavenda(); 
        } else if (columnIndex == COL_QTD) {
            return p.getVendidos(); 
        } else if (columnIndex == COL_VLR) {
            return p.getValortotal();             
        } else return "";
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public Vwvendas getVenda(int pos) {
        if (pos >= listaVenda.size())
        {    
            return null;
        } else {
            return listaVenda.get(pos);
        }
    }       
}
