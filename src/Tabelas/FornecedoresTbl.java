/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tabelas;

import entidades.Fornecedores;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author ivan.stroher
 */
public class FornecedoresTbl extends AbstractTableModel{
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
    private final int COL_COD = 0;
    
    private final int COL_NOM = 1;
    
    private final int COL_CNPJ = 2;
    
    public List<Fornecedores> listaFornecedores;

    public FornecedoresTbl(ArrayList<Fornecedores> lista) {
        listaFornecedores = lista;
    }

    @Override
    public int getRowCount() {
        return listaFornecedores.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public String getColumnName(int column) {
        
        if (column == COL_COD) {
            return "Código";
        } else if (column == COL_NOM) {
            return "Nome";
        } else if (column == COL_CNPJ) {
            return "Cnpj";
        }
        return "";
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == COL_COD) {
            return Integer.class;
        } else if (columnIndex == COL_NOM) {
            return String.class;
        } else if (columnIndex == COL_CNPJ) {
            return String.class;
        } 
        return String.class;
    }
    public int getCodigo(int rowIndex){
        Fornecedores p = listaFornecedores.get(rowIndex);
        return p.getIdfornecedor();
    }
    public Object getValueAt(int rowIndex, int columnIndex) {
        Fornecedores p = listaFornecedores.get(rowIndex);
        //verifica qual valor deve ser retornado
        if (columnIndex == COL_COD) {
            return p.getIdfornecedor();
        } else if (columnIndex == COL_NOM) {
            return p.getNome();
        } else if (columnIndex == COL_CNPJ) {
            return p.getCnpj();
        } else return "";
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public Fornecedores getFornecedor(int pos) {
        if (pos >= listaFornecedores.size())
        {    
            return null;
        } else {
            return listaFornecedores.get(pos);
        }
    }       
}
