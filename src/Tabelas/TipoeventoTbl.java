/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tabelas;

import entidades.Tipoeventos;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author ivan.stroher
 */
public class TipoeventoTbl extends AbstractTableModel{
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
    private final int COL_COD = 0;
    
    private final int COL_NOM = 1;
    
    public List<Tipoeventos> listaTipos;

    public TipoeventoTbl(ArrayList<Tipoeventos> lista) {
        listaTipos = lista;
    }

    @Override
    public int getRowCount() {
        return listaTipos.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int column) {
        
        if (column == COL_COD) {
            return "Código";
        } else if (column == COL_NOM) {
            return "Descrição";
        }
        return "";
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == COL_COD) {
            return Integer.class;
        } else if (columnIndex == COL_NOM) {
            return String.class;
        } 
        return String.class;
    }
    public int getCodigo(int rowIndex){
        Tipoeventos p = listaTipos.get(rowIndex);
        return p.getIdtipoevento();
    }
    public Object getValueAt(int rowIndex, int columnIndex) {
        Tipoeventos p = listaTipos.get(rowIndex);
        //verifica qual valor deve ser retornado
        if (columnIndex == COL_COD) {
            return p.getIdtipoevento();
        } else if (columnIndex == COL_NOM) {
            return p.getDescricao();
        } else return "";
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public Tipoeventos getTipoevento(int pos) {
        if (pos >= listaTipos.size())
        {    
            return null;
        } else {
            return listaTipos.get(pos);
        }
    }       
}
