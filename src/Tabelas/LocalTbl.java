/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tabelas;

import entidades.Locais;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author ivan.stroher
 */
public class LocalTbl extends AbstractTableModel{
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
    private final int COL_COD = 0;
    private final int COL_DSC = 1;
    private final int COL_FRN = 2;

    
    public List<Locais> listaLocais;

    public LocalTbl(ArrayList<Locais> lista) {
        listaLocais = lista;
    }

    @Override
    public int getRowCount() {
        return listaLocais.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public String getColumnName(int column) {
        
        if (column == COL_COD) {
            return "Código";
        } else if (column == COL_DSC) {
            return "Descrição";
        } else if (column == COL_FRN) {
            return "Fornecedor";            
        }
        return "";
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == COL_COD) {
            return String.class;
        } else if (columnIndex == COL_DSC) {
            return String.class;
        } else if (columnIndex == COL_FRN) {
            return String.class;            
        } 
        return String.class;
    }
    public int getCodigo(int rowIndex){
        Locais p = listaLocais.get(rowIndex);
        return p.getIdlocal();
    }
    public Object getValueAt(int rowIndex, int columnIndex) {
        Locais p = listaLocais.get(rowIndex);
        //verifica qual valor deve ser retornado
        if (columnIndex == COL_COD) {
            return p.getIdlocal();
        } else if (columnIndex == COL_DSC) {
            return p.getDescricao();
        } else if (columnIndex == COL_FRN) {
            return p.getIdfornecedor().getNome();            
        } else return "";
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public Locais getLocal(int pos) {
        if (pos >= listaLocais.size())
        {    
            return null;
        } else {
            return listaLocais.get(pos);
        }
    }       
}
