/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tabelas;

import entidades.Grupos;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author ivan.stroher
 */
public class GruposTbl extends AbstractTableModel{
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
    private final int COL_COD = 0;
    
    private final int COL_NOM = 1;
    
    
    public List<Grupos> listaGrupos;

    public GruposTbl(ArrayList<Grupos> lista) {
        listaGrupos = lista;
    }

    @Override
    public int getRowCount() {
        return listaGrupos.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int column) {
        
        if (column == COL_COD) {
            return "Código";
        } else if (column == COL_NOM) {
            return "Descrição";

        }
        return "";
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == COL_COD) {
            return Integer.class;
        } else if (columnIndex == COL_NOM) {
            return String.class;
        } 
        return String.class;
    }
    public int getCodigo(int rowIndex){
        Grupos p = listaGrupos.get(rowIndex);
        return p.getIdgrupo();
    }
    public Object getValueAt(int rowIndex, int columnIndex) {
        Grupos p = listaGrupos.get(rowIndex);
        //verifica qual valor deve ser retornado
        if (columnIndex == COL_COD) {
            return p.getIdgrupo();
        } else if (columnIndex == COL_NOM) {
            return p.getDescricao();
        } else return "";
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public Grupos getGrupo(int pos) {
        if (pos >= listaGrupos.size())
        {    
            return null;
        } else {
            return listaGrupos.get(pos);
        }
    }       
}
