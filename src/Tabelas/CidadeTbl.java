/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tabelas;

import entidades.Cidades;
import entidades.Interfaces;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author ivan.stroher
 */
public class CidadeTbl extends AbstractTableModel{
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
    private final int COL_COD = 0;
    
    private final int COL_NOM = 1;
    
    private final int COL_UF = 2;
    
    private final int COL_CEP = 3;
    
    public List<Cidades> listaCidades;

    public CidadeTbl(ArrayList<Cidades> lista) {
        listaCidades = lista;
    }

    @Override
    public int getRowCount() {
        return listaCidades.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public String getColumnName(int column) {
        
        if (column == COL_COD) {
            return "Código";
        } else if (column == COL_NOM) {
            return "Nome";
        } else if (column == COL_UF) {
            return "UF";
        }else if (column == COL_CEP) {
            return "CEP";

        }
        return "";
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == COL_COD) {
            return Integer.class;
        } else if (columnIndex == COL_NOM) {
            return String.class;
        } else if (columnIndex == COL_UF) {
            return String.class;
        } else if (columnIndex == COL_CEP) {
            return String.class;
        } 
        return String.class;
    }
    public int getCodigo(int rowIndex){
        Cidades p = listaCidades.get(rowIndex);
        return p.getIdcidade();
    }
    public Object getValueAt(int rowIndex, int columnIndex) {
        Cidades p = listaCidades.get(rowIndex);
        //verifica qual valor deve ser retornado
        if (columnIndex == COL_COD) {
            return p.getIdcidade();
        } else if (columnIndex == COL_NOM) {
            return p.getNomecidade();
        } else if (columnIndex == COL_UF) {
            return p.getUf();
        } else if (columnIndex == COL_CEP) {
            return p.getCep();            
        } else return "";
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public Cidades getCidade(int pos) {
        if (pos >= listaCidades.size())
        {    
            return null;
        } else {
            return listaCidades.get(pos);
        }
    }       
}
