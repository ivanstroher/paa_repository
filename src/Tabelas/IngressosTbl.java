/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tabelas;

import entidades.Ingressos;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author ivan.stroher
 */
public class IngressosTbl extends AbstractTableModel{
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
    private final int COL_COD = 0;
    private final int COL_EVT = 1;
    private final int COL_SET = 2;
    private final int COL_QTDE = 3;
    
    public List<Ingressos> listaIngressos;

    public IngressosTbl(ArrayList<Ingressos> lista) {
        listaIngressos = lista;
    }

    @Override
    public int getRowCount() {
        return listaIngressos.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public String getColumnName(int column) {
        
        if (column == COL_COD) {
            return "Código";
        } else if (column == COL_EVT) {
            return "Evento";
        } else if (column == COL_SET) {
            return "Setor";
        } else if (column == COL_QTDE) {
            return "Quantidade";
        }
        return "";
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == COL_COD) {
            return Integer.class;
        } else if (columnIndex == COL_EVT) {
            return String.class;
        } else if (columnIndex == COL_SET) {
            return String.class;
        } else if (columnIndex == COL_QTDE) {
            return Double.class;
        } 
        return String.class;
    }
    public int getCodigo(int rowIndex){
        Ingressos p = listaIngressos.get(rowIndex);
        return p.getIdingresso();
    }
    public Object getValueAt(int rowIndex, int columnIndex) {
        Ingressos p = listaIngressos.get(rowIndex);
        //verifica qual valor deve ser retornado
        if (columnIndex == COL_COD) {
            return p.getIdingresso();
        } else if (columnIndex == COL_EVT) {
            return p.getIdevento();
        } else if (columnIndex == COL_SET) {
            return p.getIdsetor();
        } else if (columnIndex == COL_QTDE) {
            return p.getQuantidade();
        } else return "";
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public Ingressos getIngressos(int pos) {
        if (pos >= listaIngressos.size())
        {    
            return null;
        } else {
            return listaIngressos.get(pos);
        }
    }       
}
