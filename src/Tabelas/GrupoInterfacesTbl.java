/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tabelas;

import entidades.Grupointerface;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author ivan.stroher
 */
public class GrupoInterfacesTbl extends AbstractTableModel{
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
    //private final int COL_COD = 0;
    
    private final int COL_GRP = 0;
    private final int COL_DGRP = 1;
    private final int COL_INT = 2;
    private final int COL_DINT = 3;
    
    public List<Grupointerface> listaGrupos;

    public GrupoInterfacesTbl(ArrayList<Grupointerface> lista) {
        listaGrupos = lista;
    }

    @Override
    public int getRowCount() {
        return listaGrupos.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public String getColumnName(int column) {
        
        if (column == COL_GRP) {
            return "Grupo";
        } else if (column == COL_DGRP) {
            return "Descrição";
        } else if (column == COL_INT) {
            return "Interface";
        } else if (column == COL_DINT) {
            return "Nome";            
        }
        return "";
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == COL_GRP) {
            return Integer.class;
        } else if (columnIndex == COL_DGRP) {
            return String.class;
        } else if (columnIndex == COL_INT) {
            return Integer.class;
        } else if (columnIndex == COL_DINT) {
            return String.class;
        } 
        return String.class;
    }
    public int getCodigo(int rowIndex){
        Grupointerface p = listaGrupos.get(rowIndex);
        return p.getIdgrupointerface();
    }
    public Object getValueAt(int rowIndex, int columnIndex) {
        Grupointerface p = listaGrupos.get(rowIndex);
        //verifica qual valor deve ser retornado
        if (columnIndex == COL_GRP) {
            return p.getIdgrupo();
        } else if (columnIndex == COL_DGRP) {
            return p.getIdgrupo().getDescricao();
        } else if (columnIndex == COL_INT) {
            return p.getIdinterface();
        } else if (columnIndex == COL_DGRP) {
            return p.getIdinterface().getNomeinterface();            
        } else return "";
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public Grupointerface getGrupoInterface(int pos) {
        if (pos >= listaGrupos.size())
        {    
            return null;
        } else {
            return listaGrupos.get(pos);
        }
    }       
}
