/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tabelas;

import entidades.Tabelaauditoria;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author ivan.stroher
 */
public class TabelaAuditoriaTbl extends AbstractTableModel{
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
    private final int COL_COD = 0;
    
    private final int COL_NOM = 1;
    
    public List<Tabelaauditoria> listaTabelas;

    public TabelaAuditoriaTbl(ArrayList<Tabelaauditoria> lista) {
        listaTabelas = lista;
    }
    
    @Override
    public int getRowCount() {
        return listaTabelas.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int column) {
        
        if (column == COL_COD) {
            return "Código";
        } else if (column == COL_NOM) {
            return "Tabela";
        }
        return "";
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == COL_COD) {
            return Integer.class;
        } else if (columnIndex == COL_NOM) {
            return String.class;
        } 
        return String.class;
    }
    public int getCodigo(int rowIndex){
        Tabelaauditoria p = listaTabelas.get(rowIndex);
        return p.getIdtabela();
    }
    public Object getValueAt(int rowIndex, int columnIndex) {
        Tabelaauditoria p = listaTabelas.get(rowIndex);
        //verifica qual valor deve ser retornado
        if (columnIndex == COL_COD) {
            return p.getIdtabela();
        } else if (columnIndex == COL_NOM) {
            return p.getTabela();
        } else return "";
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public Tabelaauditoria getTabela(int pos) {
        if (pos >= listaTabelas.size())
        {    
            return null;
        } else {
            return listaTabelas.get(pos);
        }
    }       
}

