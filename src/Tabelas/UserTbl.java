/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Tabelas;

import entidades.Usuarios;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author ivan.stroher
 */
public class UserTbl extends AbstractTableModel{
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
    private final int COL_NOM = 0;
    
    private final int COL_LOG = 1;
    
    private final int COL_ST = 2;
    
    private final int COL_GRP = 3;
    
    public List<Usuarios> listaUsuarios;

    public UserTbl(ArrayList<Usuarios> lista) {
        listaUsuarios = lista;
    }

    @Override
    public int getRowCount() {
        return listaUsuarios.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public String getColumnName(int column) {
        
        if (column == COL_NOM) {
            return "Nome";
        } else if (column == COL_LOG) {
            return "Login";
        } else if (column == COL_ST) {
            return "Status";
        } else if (column == COL_GRP) {
            return "Grupo";
        }
        return "";
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == COL_NOM) {
            return Integer.class;
        } else if (columnIndex == COL_LOG) {
            return String.class;
        } else if (columnIndex == COL_ST) {
            return char.class;
        } else if (columnIndex == COL_GRP) {
            return String.class;
        } 
        return String.class;
    }
    public int getCodigo(int rowIndex){
        Usuarios p = listaUsuarios.get(rowIndex);
        return p.getIdusuario();
    }
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Usuarios p = listaUsuarios.get(rowIndex);
        //verifica qual valor deve ser retornado
        if (columnIndex == COL_NOM) {
            return p.getNome();
        } else if (columnIndex == COL_LOG) {
            return p.getLogin();
        } else if (columnIndex == COL_ST) {
            return p.getStatus();
        } else if (columnIndex == COL_GRP) {
            return p.getIdgrupo();
        } else return "";
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public Usuarios getUsuario(int pos) {
        if (pos >= listaUsuarios.size())
        {    
            return null;
        } else {
            return listaUsuarios.get(pos);
        }
    }       
}
