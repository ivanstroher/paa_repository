/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tabelas;

import entidades.Interfaces;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author ivan.stroher
 */
public class InterfaceTbl extends AbstractTableModel{
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
    private final int COL_COD = 0;
    
    private final int COL_NOM = 1;
    
    public List<Interfaces> listaInterfaces;

    public InterfaceTbl(ArrayList<Interfaces> lista) {
        listaInterfaces = lista;
    }

    @Override
    public int getRowCount() {
        return listaInterfaces.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int column) {
        
        if (column == COL_COD) {
            return "Código";
        } else if (column == COL_NOM) {
            return "Nome";
        }
        return "";
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == COL_COD) {
            return Integer.class;
        } else if (columnIndex == COL_NOM) {
            return String.class;
        } 
        return String.class;
    }
    public int getCodigo(int rowIndex){
        Interfaces p = listaInterfaces.get(rowIndex);
        return p.getIdinterface();
    }
    public Object getValueAt(int rowIndex, int columnIndex) {
        Interfaces p = listaInterfaces.get(rowIndex);
        //verifica qual valor deve ser retornado
        if (columnIndex == COL_COD) {
            return p.getIdinterface();
        } else if (columnIndex == COL_NOM) {
            return p.getNomeinterface();
        } else return "";
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public Interfaces getInterface(int pos) {
        if (pos >= listaInterfaces.size())
        {    
            return null;
        } else {
            return listaInterfaces.get(pos);
        }
    }       
}
