/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Tabelas;

import entidades.Setores;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author ivan.stroher
 */
public class SetorTbl extends AbstractTableModel{
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
    private final int COL_COD = 0;
    private final int COL_DSC = 1;
    private final int COL_LOC = 2;
    private final int COL_CAP = 3;

    
    public List<Setores> listaSetores;

    public SetorTbl(ArrayList<Setores> lista) {
        listaSetores = lista;
    }

    @Override
    public int getRowCount() {
        return listaSetores.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public String getColumnName(int column) {
        
        if (column == COL_COD) {
            return "Código";
        } else if (column == COL_DSC) {
            return "Descrição";
        } else if (column == COL_LOC) {
            return "Local";     
        } else if (column == COL_CAP) {
            return "Capacidade";             
        }
        return "";
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == COL_COD) {
            return String.class;
        } else if (columnIndex == COL_DSC) {
            return String.class;
        } else if (columnIndex == COL_LOC) {
            return String.class; 
        } else if (columnIndex == COL_CAP) {
            return String.class;             
        } 
        return String.class;
    }
    public int getCodigo(int rowIndex){
        Setores p = listaSetores.get(rowIndex);
        return p.getIdsetor();
    }
    public Object getValueAt(int rowIndex, int columnIndex) {
        Setores p = listaSetores.get(rowIndex);
        //verifica qual valor deve ser retornado
        if (columnIndex == COL_COD) {
            return p.getIdlocal();
        } else if (columnIndex == COL_DSC) {
            return p.getDescricao();
        } else if (columnIndex == COL_LOC) {
            return p.getIdlocal().getDescricao();  
        } else if (columnIndex == COL_CAP) {
            return p.getCapacidade();              
        } else return "";
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public Setores getSetor(int pos) {
        if (pos >= listaSetores.size())
        {    
            return null;
        } else {
            return listaSetores.get(pos);
        }
    }       
}
