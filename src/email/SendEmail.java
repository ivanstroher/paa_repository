/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package email;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

/**
 *
 * @author Lazaro
 */
public class SendEmail {
    
    public void sendEmail() throws EmailException {
    
        SimpleEmail email = new SimpleEmail();
        //Utilize o hostname do seu provedor de email
        System.out.println("alterando hostname...");
        email.setHostName("smtp.gmail.com");
        //Quando a porta utilizada não é a padrão (gmail = 465)
        email.setSmtpPort(465);
        //Adicione os destinatários
        //email.addTo("xxx@xxx.com", "Jose");
        email.addTo("ivanstroher@gmail.com", "Ivan");
        //Configure o seu email do qual enviará
        //email.setFrom("seuemail@seuprovedor.com", "seu nome");
        email.setFrom("ivanstroher@gmail.com", "Ivan");
        //Adicione um assunto
        //email.setSubject("Test message");
        email.setSubject("Teste de Assunto");
        //Adicione a mensagem do email
        //email.setMsg("This is a simple test of commons-email");
        email.setMsg("Esse é um teste de conteúdo.");
        
        //Para autenticar no servidor é necessário chamar os dois métodos abaixo
        System.out.println("autenticando...");
        //email.setTLS(false);
        //email.setSSL(false);
        email.setSSL(true);
        //Adicione seu nome de usuário e senha
        email.setAuthentication("ivanstroher@gmail.com", "caminhoderosario2012");
        System.out.println("enviando...");
        email.send();
        System.out.println("Email enviado!");
    }
}
