/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ivan.stroher
 */
@Entity
@Table(name = "vendas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vendas.findAll", query = "SELECT v FROM Vendas v"),
    @NamedQuery(name = "Vendas.findByIdvenda", query = "SELECT v FROM Vendas v WHERE v.idvenda = :idvenda"),
    @NamedQuery(name = "Vendas.findByDatavenda", query = "SELECT v FROM Vendas v WHERE v.datavenda = :datavenda"),
    @NamedQuery(name = "Vendas.findByValortotal", query = "SELECT v FROM Vendas v WHERE v.valortotal = :valortotal"),
    @NamedQuery(name = "Vendas.findByIdingresso", query = "SELECT v FROM Vendas v WHERE v.idingresso = :idingresso"),
    @NamedQuery(name = "Vendas.findByQtde", query = "SELECT v FROM Vendas v WHERE v.qtde = :qtde")})
public class Vendas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idvenda")
    private Integer idvenda;
    @Basic(optional = false)
    @Column(name = "datavenda")
    @Temporal(TemporalType.DATE)
    private Date datavenda;
    @Basic(optional = false)
    @Column(name = "valortotal")
    private double valortotal;
    @Column(name = "idingresso")
    private Integer idingresso;
    @Column(name = "qtde")
    private Integer qtde;
    @JoinColumn(name = "idevento", referencedColumnName = "idevento")
    @ManyToOne(optional = false)
    private Eventos idevento;

    public Vendas() {
    }

    public Vendas(Integer idvenda) {
        this.idvenda = idvenda;
    }

    public Vendas(Integer idvenda, Date datavenda, double valortotal) {
        this.idvenda = idvenda;
        this.datavenda = datavenda;
        this.valortotal = valortotal;
    }

    public Integer getIdvenda() {
        return idvenda;
    }

    public void setIdvenda(Integer idvenda) {
        this.idvenda = idvenda;
    }

    public Date getDatavenda() {
        return datavenda;
    }

    public void setDatavenda(Date datavenda) {
        this.datavenda = datavenda;
    }

    public double getValortotal() {
        return valortotal;
    }

    public void setValortotal(double valortotal) {
        this.valortotal = valortotal;
    }

    public Integer getIdingresso() {
        return idingresso;
    }

    public void setIdingresso(Integer idingresso) {
        this.idingresso = idingresso;
    }

    public Integer getQtde() {
        return qtde;
    }

    public void setQtde(Integer qtde) {
        this.qtde = qtde;
    }

    public Eventos getIdevento() {
        return idevento;
    }

    public void setIdevento(Eventos idevento) {
        this.idevento = idevento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idvenda != null ? idvenda.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vendas)) {
            return false;
        }
        Vendas other = (Vendas) object;
        if ((this.idvenda == null && other.idvenda != null) || (this.idvenda != null && !this.idvenda.equals(other.idvenda))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Vendas[ idvenda=" + idvenda + " ]";
    }
    
}
