/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ivan.stroher
 */
@Entity
@Table(name = "fornecedores")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Fornecedores.findAll", query = "SELECT f FROM Fornecedores f"),
    @NamedQuery(name = "Fornecedores.findByIdfornecedor", query = "SELECT f FROM Fornecedores f WHERE f.idfornecedor = :idfornecedor"),
    @NamedQuery(name = "Fornecedores.findByNome", query = "SELECT f FROM Fornecedores f WHERE f.nome = :nome"),
    @NamedQuery(name = "Fornecedores.findByCnpj", query = "SELECT f FROM Fornecedores f WHERE f.cnpj = :cnpj"),
    @NamedQuery(name = "Fornecedores.findByEmail", query = "SELECT f FROM Fornecedores f WHERE f.email = :email"),
    @NamedQuery(name = "Fornecedores.findByTelefone", query = "SELECT f FROM Fornecedores f WHERE f.telefone = :telefone"),
    @NamedQuery(name = "Fornecedores.findByTelefone2", query = "SELECT f FROM Fornecedores f WHERE f.telefone2 = :telefone2")})
public class Fornecedores implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idfornecedor")
    private Integer idfornecedor;
    @Basic(optional = false)
    @Column(name = "nome")
    private String nome;
    @Basic(optional = false)
    @Column(name = "cnpj")
    private String cnpj;
    @Basic(optional = false)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @Column(name = "telefone")
    private String telefone;
    @Basic(optional = false)
    @Column(name = "telefone2")
    private String telefone2;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idfornecedor")
    private Collection<Eventos> eventosCollection;
    @JoinColumn(name = "idtipoevento", referencedColumnName = "idtipoevento")
    @ManyToOne
    private Tipoeventos idtipoevento;
    @JoinColumn(name = "idcidade", referencedColumnName = "idcidade")
    @ManyToOne
    private Cidades idcidade;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idfornecedor")
    private Collection<Locais> locaisCollection;

    public Fornecedores() {
    }

    public Fornecedores(Integer idfornecedor) {
        this.idfornecedor = idfornecedor;
    }

    public Fornecedores(Integer idfornecedor, String nome, String cnpj, String email, String telefone, String telefone2) {
        this.idfornecedor = idfornecedor;
        this.nome = nome;
        this.cnpj = cnpj;
        this.email = email;
        this.telefone = telefone;
        this.telefone2 = telefone2;
    }

    public Integer getIdfornecedor() {
        return idfornecedor;
    }

    public void setIdfornecedor(Integer idfornecedor) {
        this.idfornecedor = idfornecedor;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getTelefone2() {
        return telefone2;
    }

    public void setTelefone2(String telefone2) {
        this.telefone2 = telefone2;
    }

    @XmlTransient
    public Collection<Eventos> getEventosCollection() {
        return eventosCollection;
    }

    public void setEventosCollection(Collection<Eventos> eventosCollection) {
        this.eventosCollection = eventosCollection;
    }

    public Tipoeventos getIdtipoevento() {
        return idtipoevento;
    }

    public void setIdtipoevento(Tipoeventos idtipoevento) {
        this.idtipoevento = idtipoevento;
    }

    public Cidades getIdcidade() {
        return idcidade;
    }

    public void setIdcidade(Cidades idcidade) {
        this.idcidade = idcidade;
    }

    @XmlTransient
    public Collection<Locais> getLocaisCollection() {
        return locaisCollection;
    }

    public void setLocaisCollection(Collection<Locais> locaisCollection) {
        this.locaisCollection = locaisCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idfornecedor != null ? idfornecedor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Fornecedores)) {
            return false;
        }
        Fornecedores other = (Fornecedores) object;
        if ((this.idfornecedor == null && other.idfornecedor != null) || (this.idfornecedor != null && !this.idfornecedor.equals(other.idfornecedor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Fornecedores[ idfornecedor=" + idfornecedor + " ]";
    }
    
}
