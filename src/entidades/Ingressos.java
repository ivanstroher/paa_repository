/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ivan.stroher
 */
@Entity
@Table(name = "ingressos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ingressos.findAll", query = "SELECT i FROM Ingressos i"),
    @NamedQuery(name = "Ingressos.findByIdingresso", query = "SELECT i FROM Ingressos i WHERE i.idingresso = :idingresso"),
    @NamedQuery(name = "Ingressos.findByQuantidade", query = "SELECT i FROM Ingressos i WHERE i.quantidade = :quantidade"),
    @NamedQuery(name = "Ingressos.findByPrecounitario", query = "SELECT i FROM Ingressos i WHERE i.precounitario = :precounitario")})
public class Ingressos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idingresso")
    private Integer idingresso;
    @Column(name = "quantidade")
    private Integer quantidade;
    @Basic(optional = false)
    @Column(name = "precounitario")
    private double precounitario;
    @JoinColumn(name = "idsetor", referencedColumnName = "idsetor")
    @OneToOne(optional = false)
    private Setores idsetor;
    @JoinColumn(name = "idevento", referencedColumnName = "idevento")
    @ManyToOne(optional = false)
    private Eventos idevento;

    public Ingressos() {
    }

    public Ingressos(Integer idingresso) {
        this.idingresso = idingresso;
    }

    public Ingressos(Integer idingresso, double precounitario) {
        this.idingresso = idingresso;
        this.precounitario = precounitario;
    }

    public Integer getIdingresso() {
        return idingresso;
    }

    public void setIdingresso(Integer idingresso) {
        this.idingresso = idingresso;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public double getPrecounitario() {
        return precounitario;
    }

    public void setPrecounitario(double precounitario) {
        this.precounitario = precounitario;
    }

    public Setores getIdsetor() {
        return idsetor;
    }

    public void setIdsetor(Setores idsetor) {
        this.idsetor = idsetor;
    }

    public Eventos getIdevento() {
        return idevento;
    }

    public void setIdevento(Eventos idevento) {
        this.idevento = idevento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idingresso != null ? idingresso.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ingressos)) {
            return false;
        }
        Ingressos other = (Ingressos) object;
        if ((this.idingresso == null && other.idingresso != null) || (this.idingresso != null && !this.idingresso.equals(other.idingresso))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Ingressos[ idingresso=" + idingresso + " ]";
    }
    
}
