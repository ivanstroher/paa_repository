/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ivan.stroher
 */
@Entity
@Table(name = "tipoeventos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tipoeventos.findAll", query = "SELECT t FROM Tipoeventos t"),
    @NamedQuery(name = "Tipoeventos.findByIdtipoevento", query = "SELECT t FROM Tipoeventos t WHERE t.idtipoevento = :idtipoevento"),
    @NamedQuery(name = "Tipoeventos.findByDescricao", query = "SELECT t FROM Tipoeventos t WHERE t.descricao = :descricao")})
public class Tipoeventos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idtipoevento")
    private Integer idtipoevento;
    @Basic(optional = false)
    @Column(name = "descricao")
    private String descricao;
    @OneToMany(mappedBy = "idtipoevento")
    private Collection<Fornecedores> fornecedoresCollection;

    public Tipoeventos() {
    }

    public Tipoeventos(Integer idtipoevento) {
        this.idtipoevento = idtipoevento;
    }

    public Tipoeventos(Integer idtipoevento, String descricao) {
        this.idtipoevento = idtipoevento;
        this.descricao = descricao;
    }

    public Integer getIdtipoevento() {
        return idtipoevento;
    }

    public void setIdtipoevento(Integer idtipoevento) {
        this.idtipoevento = idtipoevento;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @XmlTransient
    public Collection<Fornecedores> getFornecedoresCollection() {
        return fornecedoresCollection;
    }

    public void setFornecedoresCollection(Collection<Fornecedores> fornecedoresCollection) {
        this.fornecedoresCollection = fornecedoresCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtipoevento != null ? idtipoevento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tipoeventos)) {
            return false;
        }
        Tipoeventos other = (Tipoeventos) object;
        if ((this.idtipoevento == null && other.idtipoevento != null) || (this.idtipoevento != null && !this.idtipoevento.equals(other.idtipoevento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Tipoeventos[ idtipoevento=" + idtipoevento + " ]";
    }
    
}
