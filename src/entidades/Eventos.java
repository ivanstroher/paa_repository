/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ivan.stroher
 */
@Entity
@Table(name = "eventos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Eventos.findAll", query = "SELECT e FROM Eventos e"),
    @NamedQuery(name = "Eventos.findByIdevento", query = "SELECT e FROM Eventos e WHERE e.idevento = :idevento"),
    @NamedQuery(name = "Eventos.findByDescricao", query = "SELECT e FROM Eventos e WHERE e.descricao = :descricao"),
    @NamedQuery(name = "Eventos.findByDataevento", query = "SELECT e FROM Eventos e WHERE e.dataevento = :dataevento"),
    @NamedQuery(name = "Eventos.findByHoraevento", query = "SELECT e FROM Eventos e WHERE e.horaevento = :horaevento"),
    @NamedQuery(name = "Eventos.findByTotalingressos", query = "SELECT e FROM Eventos e WHERE e.totalingressos = :totalingressos")})
public class Eventos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idevento")
    private Integer idevento;
    @Basic(optional = false)
    @Column(name = "descricao")
    private String descricao;
    @Basic(optional = false)
    @Column(name = "dataevento")
    private String dataevento;
    @Basic(optional = false)
    @Column(name = "horaevento")
    private String horaevento;
    @Basic(optional = false)
    @Column(name = "totalingressos")
    private int totalingressos;
    @JoinColumn(name = "idlocal", referencedColumnName = "idlocal")
    @ManyToOne(optional = false)
    private Locais idlocal;
    @JoinColumn(name = "idfornecedor", referencedColumnName = "idfornecedor")
    @ManyToOne(optional = false)
    private Fornecedores idfornecedor;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idevento")
    private Collection<Ingressos> ingressosCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idevento")
    private Collection<Vendas> vendasCollection;

    public Eventos() {
    }

    public Eventos(Integer idevento) {
        this.idevento = idevento;
    }

    public Eventos(Integer idevento, String descricao, String dataevento, String horaevento, int totalingressos) {
        this.idevento = idevento;
        this.descricao = descricao;
        this.dataevento = dataevento;
        this.horaevento = horaevento;
        this.totalingressos = totalingressos;
    }

    public Integer getIdevento() {
        return idevento;
    }

    public void setIdevento(Integer idevento) {
        this.idevento = idevento;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getDataevento() {
        return dataevento;
    }

    public void setDataevento(String dataevento) {
        this.dataevento = dataevento;
    }

    public String getHoraevento() {
        return horaevento;
    }

    public void setHoraevento(String horaevento) {
        this.horaevento = horaevento;
    }

    public int getTotalingressos() {
        return totalingressos;
    }

    public void setTotalingressos(int totalingressos) {
        this.totalingressos = totalingressos;
    }

    public Locais getIdlocal() {
        return idlocal;
    }

    public void setIdlocal(Locais idlocal) {
        this.idlocal = idlocal;
    }

    public Fornecedores getIdfornecedor() {
        return idfornecedor;
    }

    public void setIdfornecedor(Fornecedores idfornecedor) {
        this.idfornecedor = idfornecedor;
    }

    @XmlTransient
    public Collection<Ingressos> getIngressosCollection() {
        return ingressosCollection;
    }

    public void setIngressosCollection(Collection<Ingressos> ingressosCollection) {
        this.ingressosCollection = ingressosCollection;
    }

    @XmlTransient
    public Collection<Vendas> getVendasCollection() {
        return vendasCollection;
    }

    public void setVendasCollection(Collection<Vendas> vendasCollection) {
        this.vendasCollection = vendasCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idevento != null ? idevento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Eventos)) {
            return false;
        }
        Eventos other = (Eventos) object;
        if ((this.idevento == null && other.idevento != null) || (this.idevento != null && !this.idevento.equals(other.idevento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Eventos[ idevento=" + idevento + " ]";
    }
    
}
