/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ivan.stroher
 */
@Entity
@Table(name = "vwlogs")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vwlogs.findAll", query = "SELECT v FROM Vwlogs v"),
    @NamedQuery(name = "Vwlogs.findByIdlog", query = "SELECT v FROM Vwlogs v WHERE v.idlog = :idlog"),
    @NamedQuery(name = "Vwlogs.findByIdusuariolog", query = "SELECT v FROM Vwlogs v WHERE v.idusuariolog = :idusuariolog"),
    @NamedQuery(name = "Vwlogs.findByNomeusuario", query = "SELECT v FROM Vwlogs v WHERE v.nomeusuario = :nomeusuario"),
    @NamedQuery(name = "Vwlogs.findByLogin", query = "SELECT v FROM Vwlogs v WHERE v.login = :login"),
    @NamedQuery(name = "Vwlogs.findByDatalog", query = "SELECT v FROM Vwlogs v WHERE v.datalog = :datalog"),
    @NamedQuery(name = "Vwlogs.findByIdtabelalog", query = "SELECT v FROM Vwlogs v WHERE v.idtabelalog = :idtabelalog"),
    @NamedQuery(name = "Vwlogs.findByTabelalog", query = "SELECT v FROM Vwlogs v WHERE v.tabelalog = :tabelalog"),
    @NamedQuery(name = "Vwlogs.findByValor", query = "SELECT v FROM Vwlogs v WHERE v.valor = :valor")})
public class Vwlogs implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "idlog")
    private Integer idlog;
    @Column(name = "idusuariolog")
    private Integer idusuariolog;
    @Column(name = "nomeusuario")
    private String nomeusuario;
    @Column(name = "login")
    private String login;
    @Column(name = "datalog")
    private String datalog;
    @Column(name = "idtabelalog")
    private Integer idtabelalog;
    @Column(name = "tabelalog")
    private String tabelalog;
    @Column(name = "valor")
    private String valor;

    public Vwlogs() {
    }

    public Integer getIdlog() {
        return idlog;
    }

    public void setIdlog(Integer idlog) {
        this.idlog = idlog;
    }

    public Integer getIdusuariolog() {
        return idusuariolog;
    }

    public void setIdusuariolog(Integer idusuariolog) {
        this.idusuariolog = idusuariolog;
    }

    public String getNomeusuario() {
        return nomeusuario;
    }

    public void setNomeusuario(String nomeusuario) {
        this.nomeusuario = nomeusuario;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getDatalog() {
        return datalog;
    }

    public void setDatalog(String datalog) {
        this.datalog = datalog;
    }

    public Integer getIdtabelalog() {
        return idtabelalog;
    }

    public void setIdtabelalog(Integer idtabelalog) {
        this.idtabelalog = idtabelalog;
    }

    public String getTabelalog() {
        return tabelalog;
    }

    public void setTabelalog(String tabelalog) {
        this.tabelalog = tabelalog;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
    
}
