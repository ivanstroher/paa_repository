/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ivan.stroher
 */
@Entity
@Table(name = "locais")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Locais.findAll", query = "SELECT l FROM Locais l"),
    @NamedQuery(name = "Locais.findByIdlocal", query = "SELECT l FROM Locais l WHERE l.idlocal = :idlocal"),
    @NamedQuery(name = "Locais.findByDescricao", query = "SELECT l FROM Locais l WHERE l.descricao = :descricao")})
public class Locais implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idlocal")
    private Integer idlocal;
    @Basic(optional = false)
    @Column(name = "descricao")
    private String descricao;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idlocal")
    private Collection<Eventos> eventosCollection;
    @JoinColumn(name = "idfornecedor", referencedColumnName = "idfornecedor")
    @ManyToOne(optional = false)
    private Fornecedores idfornecedor;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idlocal")
    private Collection<Setores> setoresCollection;

    public Locais() {
    }

    public Locais(Integer idlocal) {
        this.idlocal = idlocal;
    }

    public Locais(Integer idlocal, String descricao) {
        this.idlocal = idlocal;
        this.descricao = descricao;
    }

    public Integer getIdlocal() {
        return idlocal;
    }

    public void setIdlocal(Integer idlocal) {
        this.idlocal = idlocal;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @XmlTransient
    public Collection<Eventos> getEventosCollection() {
        return eventosCollection;
    }

    public void setEventosCollection(Collection<Eventos> eventosCollection) {
        this.eventosCollection = eventosCollection;
    }

    public Fornecedores getIdfornecedor() {
        return idfornecedor;
    }

    public void setIdfornecedor(Fornecedores idfornecedor) {
        this.idfornecedor = idfornecedor;
    }

    @XmlTransient
    public Collection<Setores> getSetoresCollection() {
        return setoresCollection;
    }

    public void setSetoresCollection(Collection<Setores> setoresCollection) {
        this.setoresCollection = setoresCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idlocal != null ? idlocal.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Locais)) {
            return false;
        }
        Locais other = (Locais) object;
        if ((this.idlocal == null && other.idlocal != null) || (this.idlocal != null && !this.idlocal.equals(other.idlocal))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Locais[ idlocal=" + idlocal + " ]";
    }
    
}
