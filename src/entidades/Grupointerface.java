/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ivan.stroher
 */
@Entity
@Table(name = "grupointerface")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Grupointerface.findAll", query = "SELECT g FROM Grupointerface g"),
    @NamedQuery(name = "Grupointerface.findByIdgrupointerface", query = "SELECT g FROM Grupointerface g WHERE g.idgrupointerface = :idgrupointerface")})
public class Grupointerface implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idgrupointerface")
    private Integer idgrupointerface;
    @JoinColumn(name = "idinterface", referencedColumnName = "idinterface")
    @ManyToOne(optional = false)
    private Interfaces idinterface;
    @JoinColumn(name = "idgrupo", referencedColumnName = "idgrupo")
    @ManyToOne(optional = false)
    private Grupos idgrupo;

    public Grupointerface() {
    }

    public Grupointerface(Integer idgrupointerface) {
        this.idgrupointerface = idgrupointerface;
    }

    public Integer getIdgrupointerface() {
        return idgrupointerface;
    }

    public void setIdgrupointerface(Integer idgrupointerface) {
        this.idgrupointerface = idgrupointerface;
    }

    public Interfaces getIdinterface() {
        return idinterface;
    }

    public void setIdinterface(Interfaces idinterface) {
        this.idinterface = idinterface;
    }

    public Grupos getIdgrupo() {
        return idgrupo;
    }

    public void setIdgrupo(Grupos idgrupo) {
        this.idgrupo = idgrupo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idgrupointerface != null ? idgrupointerface.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Grupointerface)) {
            return false;
        }
        Grupointerface other = (Grupointerface) object;
        if ((this.idgrupointerface == null && other.idgrupointerface != null) || (this.idgrupointerface != null && !this.idgrupointerface.equals(other.idgrupointerface))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Grupointerface[ idgrupointerface=" + idgrupointerface + " ]";
    }
    
}
