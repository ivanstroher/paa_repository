/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ivan.stroher
 */
@Entity
@Table(name = "tabelaauditoria")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tabelaauditoria.findAll", query = "SELECT t FROM Tabelaauditoria t"),
    @NamedQuery(name = "Tabelaauditoria.findByIdtabela", query = "SELECT t FROM Tabelaauditoria t WHERE t.idtabela = :idtabela"),
    @NamedQuery(name = "Tabelaauditoria.findByTabela", query = "SELECT t FROM Tabelaauditoria t WHERE t.tabela = :tabela"),
    @NamedQuery(name = "Tabelaauditoria.findByHabilitado", query = "SELECT t FROM Tabelaauditoria t WHERE t.habilitado = :habilitado")})
public class Tabelaauditoria implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idtabela")
    private Integer idtabela;
    @Basic(optional = false)
    @Column(name = "tabela")
    private String tabela;
    @Basic(optional = false)
    @Column(name = "habilitado")
    private String habilitado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idtabelaauditoria")
    private Collection<Auditorias> auditoriasCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idtabelalog")
    private Collection<Log> logCollection;

    public Tabelaauditoria() {
    }

    public Tabelaauditoria(Integer idtabela) {
        this.idtabela = idtabela;
    }

    public Tabelaauditoria(Integer idtabela, String tabela, String habilitado) {
        this.idtabela = idtabela;
        this.tabela = tabela;
        this.habilitado = habilitado;
    }

    public Integer getIdtabela() {
        return idtabela;
    }

    public void setIdtabela(Integer idtabela) {
        this.idtabela = idtabela;
    }

    public String getTabela() {
        return tabela;
    }

    public void setTabela(String tabela) {
        this.tabela = tabela;
    }

    public String getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(String habilitado) {
        this.habilitado = habilitado;
    }

    @XmlTransient
    public Collection<Auditorias> getAuditoriasCollection() {
        return auditoriasCollection;
    }

    public void setAuditoriasCollection(Collection<Auditorias> auditoriasCollection) {
        this.auditoriasCollection = auditoriasCollection;
    }

    @XmlTransient
    public Collection<Log> getLogCollection() {
        return logCollection;
    }

    public void setLogCollection(Collection<Log> logCollection) {
        this.logCollection = logCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtabela != null ? idtabela.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tabelaauditoria)) {
            return false;
        }
        Tabelaauditoria other = (Tabelaauditoria) object;
        if ((this.idtabela == null && other.idtabela != null) || (this.idtabela != null && !this.idtabela.equals(other.idtabela))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Tabelaauditoria[ idtabela=" + idtabela + " ]";
    }
    
}
