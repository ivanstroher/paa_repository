/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ivan.stroher
 */
@Entity
@Table(name = "setores")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Setores.findAll", query = "SELECT s FROM Setores s"),
    @NamedQuery(name = "Setores.findByIdsetor", query = "SELECT s FROM Setores s WHERE s.idsetor = :idsetor"),
    @NamedQuery(name = "Setores.findByDescricao", query = "SELECT s FROM Setores s WHERE s.descricao = :descricao"),
    @NamedQuery(name = "Setores.findByCapacidade", query = "SELECT s FROM Setores s WHERE s.capacidade = :capacidade")})
public class Setores implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idsetor")
    private Integer idsetor;
    @Basic(optional = false)
    @Column(name = "descricao")
    private String descricao;
    @Basic(optional = false)
    @Column(name = "capacidade")
    private int capacidade;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "idsetor")
    private Ingressos ingressos;
    @JoinColumn(name = "idlocal", referencedColumnName = "idlocal")
    @ManyToOne(optional = false)
    private Locais idlocal;

    public Setores() {
    }

    public Setores(Integer idsetor) {
        this.idsetor = idsetor;
    }

    public Setores(Integer idsetor, String descricao, int capacidade) {
        this.idsetor = idsetor;
        this.descricao = descricao;
        this.capacidade = capacidade;
    }

    public Integer getIdsetor() {
        return idsetor;
    }

    public void setIdsetor(Integer idsetor) {
        this.idsetor = idsetor;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getCapacidade() {
        return capacidade;
    }

    public void setCapacidade(int capacidade) {
        this.capacidade = capacidade;
    }

    public Ingressos getIngressos() {
        return ingressos;
    }

    public void setIngressos(Ingressos ingressos) {
        this.ingressos = ingressos;
    }

    public Locais getIdlocal() {
        return idlocal;
    }

    public void setIdlocal(Locais idlocal) {
        this.idlocal = idlocal;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idsetor != null ? idsetor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Setores)) {
            return false;
        }
        Setores other = (Setores) object;
        if ((this.idsetor == null && other.idsetor != null) || (this.idsetor != null && !this.idsetor.equals(other.idsetor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Setores[ idsetor=" + idsetor + " ]";
    }
    
}
