/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ivan.stroher
 */
@Entity
@Table(name = "interfaces")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Interfaces.findAll", query = "SELECT i FROM Interfaces i"),
    @NamedQuery(name = "Interfaces.findByIdinterface", query = "SELECT i FROM Interfaces i WHERE i.idinterface = :idinterface"),
    @NamedQuery(name = "Interfaces.findByNomeinterface", query = "SELECT i FROM Interfaces i WHERE i.nomeinterface = :nomeinterface")})
public class Interfaces implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idinterface")
    private Integer idinterface;
    @Column(name = "nomeinterface")
    private String nomeinterface;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idinterface")
    private Collection<Grupointerface> grupointerfaceCollection;

    public Interfaces() {
    }

    public Interfaces(Integer idinterface) {
        this.idinterface = idinterface;
    }

    public Integer getIdinterface() {
        return idinterface;
    }

    public void setIdinterface(Integer idinterface) {
        this.idinterface = idinterface;
    }

    public String getNomeinterface() {
        return nomeinterface;
    }

    public void setNomeinterface(String nomeinterface) {
        this.nomeinterface = nomeinterface;
    }

    @XmlTransient
    public Collection<Grupointerface> getGrupointerfaceCollection() {
        return grupointerfaceCollection;
    }

    public void setGrupointerfaceCollection(Collection<Grupointerface> grupointerfaceCollection) {
        this.grupointerfaceCollection = grupointerfaceCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idinterface != null ? idinterface.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Interfaces)) {
            return false;
        }
        Interfaces other = (Interfaces) object;
        if ((this.idinterface == null && other.idinterface != null) || (this.idinterface != null && !this.idinterface.equals(other.idinterface))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Interfaces[ idinterface=" + idinterface + " ]";
    }
    
}
