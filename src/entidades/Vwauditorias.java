/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ivan.stroher
 */
@Entity
@Table(name = "vwauditorias")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vwauditorias.findAll", query = "SELECT v FROM Vwauditorias v"),
    @NamedQuery(name = "Vwauditorias.findByIdauditoria", query = "SELECT v FROM Vwauditorias v WHERE v.idauditoria = :idauditoria"),
    @NamedQuery(name = "Vwauditorias.findByIdusuarioauditoria", query = "SELECT v FROM Vwauditorias v WHERE v.idusuarioauditoria = :idusuarioauditoria"),
    @NamedQuery(name = "Vwauditorias.findByNomeusuario", query = "SELECT v FROM Vwauditorias v WHERE v.nomeusuario = :nomeusuario"),
    @NamedQuery(name = "Vwauditorias.findByLogin", query = "SELECT v FROM Vwauditorias v WHERE v.login = :login"),
    @NamedQuery(name = "Vwauditorias.findByDataauditoria", query = "SELECT v FROM Vwauditorias v WHERE v.dataauditoria = :dataauditoria"),
    @NamedQuery(name = "Vwauditorias.findByTipoauditoria", query = "SELECT v FROM Vwauditorias v WHERE v.tipoauditoria = :tipoauditoria"),
    @NamedQuery(name = "Vwauditorias.findByIdtabelaauditoria", query = "SELECT v FROM Vwauditorias v WHERE v.idtabelaauditoria = :idtabelaauditoria"),
    @NamedQuery(name = "Vwauditorias.findByTabela", query = "SELECT v FROM Vwauditorias v WHERE v.tabela = :tabela"),
    @NamedQuery(name = "Vwauditorias.findByValorauditoria", query = "SELECT v FROM Vwauditorias v WHERE v.valorauditoria = :valorauditoria")})
public class Vwauditorias implements Serializable {


    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "idauditoria")
    private Integer idauditoria;
    @Column(name = "idusuarioauditoria")
    private Integer idusuarioauditoria;
    @Column(name = "nomeusuario")
    private String nomeusuario;
    @Column(name = "login")
    private String login;
    @Column(name = "dataauditoria")
    private String dataauditoria;
    @Column(name = "tipoauditoria")
    private String tipoauditoria;
    @Column(name = "idtabelaauditoria")
    private Integer idtabelaauditoria;
    @Column(name = "tabela")
    private String tabela;
    @Column(name = "valorauditoria")
    private String valorauditoria;

    public Vwauditorias() {
    }

    public Integer getIdauditoria() {
        return idauditoria;
    }

    public void setIdauditoria(Integer idauditoria) {
        this.idauditoria = idauditoria;
    }

    public Integer getIdusuarioauditoria() {
        return idusuarioauditoria;
    }

    public void setIdusuarioauditoria(Integer idusuarioauditoria) {
        this.idusuarioauditoria = idusuarioauditoria;
    }

    public String getNomeusuario() {
        return nomeusuario;
    }

    public void setNomeusuario(String nomeusuario) {
        this.nomeusuario = nomeusuario;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getDataauditoria() {
        return dataauditoria;
    }

    public void setDataauditoria(String dataauditoria) {
        this.dataauditoria = dataauditoria;
    }

    public String getTipoauditoria() {
        return tipoauditoria;
    }

    public void setTipoauditoria(String tipoauditoria) {
        this.tipoauditoria = tipoauditoria;
    }

    public Integer getIdtabelaauditoria() {
        return idtabelaauditoria;
    }

    public void setIdtabelaauditoria(Integer idtabelaauditoria) {
        this.idtabelaauditoria = idtabelaauditoria;
    }

    public String getTabela() {
        return tabela;
    }

    public void setTabela(String tabela) {
        this.tabela = tabela;
    }

    public String getValorauditoria() {
        return valorauditoria;
    }

    public void setValorauditoria(String valorauditoria) {
        this.valorauditoria = valorauditoria;
    }
    
}
