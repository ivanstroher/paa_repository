/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ivan.stroher
 */
@Entity
@Table(name = "auditorias")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Auditorias.findAll", query = "SELECT a FROM Auditorias a"),
    @NamedQuery(name = "Auditorias.findByIdauditoria", query = "SELECT a FROM Auditorias a WHERE a.idauditoria = :idauditoria"),
    @NamedQuery(name = "Auditorias.findByDataauditoria", query = "SELECT a FROM Auditorias a WHERE a.dataauditoria = :dataauditoria"),
    @NamedQuery(name = "Auditorias.findByTipoauditoria", query = "SELECT a FROM Auditorias a WHERE a.tipoauditoria = :tipoauditoria"),
    @NamedQuery(name = "Auditorias.findByValorauditoria", query = "SELECT a FROM Auditorias a WHERE a.valorauditoria = :valorauditoria")})
public class Auditorias implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idauditoria")
    private Integer idauditoria;
    @Basic(optional = false)
    @Column(name = "dataauditoria")
    private String dataauditoria;
    @Basic(optional = false)
    @Column(name = "tipoauditoria")
    private String tipoauditoria;
    @Basic(optional = false)
    @Column(name = "valorauditoria")
    private String valorauditoria;
    @Basic(optional = false)
    @Column(name = "idtabelaauditoria")
    private int idtabelaauditoria;
    @Basic(optional = false)
    @Column(name = "idusuarioauditoria")
    private int idusuarioauditoria;    
//    @JoinColumn(name = "idusuarioauditoria", referencedColumnName = "idusuario")
//    @ManyToOne(optional = false)
//    private Usuarios idusuarioauditoria;

//    @JoinColumn(name = "idtabelaauditoria", referencedColumnName = "idtabela")
//    @ManyToOne(optional = false)
//    private Tabelaauditoria idtabelaauditoria;

    public Auditorias() {
    }

    public Auditorias(Integer idauditoria) {
        this.idauditoria = idauditoria;
    }

    public Auditorias(Integer idauditoria, String dataauditoria, String tipoauditoria, String valorauditoria) {
        this.idauditoria = idauditoria;
        this.dataauditoria = dataauditoria;
        this.tipoauditoria = tipoauditoria;
        this.valorauditoria = valorauditoria;
    }

    public Integer getIdauditoria() {
        return idauditoria;
    }

    public void setIdauditoria(Integer idauditoria) {
        this.idauditoria = idauditoria;
    }

    public String getDataauditoria() {
        return dataauditoria;
    }

    public void setDataauditoria(String dataauditoria) {
        this.dataauditoria = dataauditoria;
    }

    public String getTipoauditoria() {
        return tipoauditoria;
    }

    public void setTipoauditoria(String tipoauditoria) {
        this.tipoauditoria = tipoauditoria;
    }

    public String getValorauditoria() {
        return valorauditoria;
    }

    public void setValorauditoria(String valorauditoria) {
        this.valorauditoria = valorauditoria;
    }

    public int getIdusuarioauditoria() {
        return idusuarioauditoria;
    }

    public void setIdusuarioauditoria(int idusuarioauditoria) {
        this.idusuarioauditoria = idusuarioauditoria;
    }

    public int getIdtabelaauditoria() {
        return idtabelaauditoria;
    }

    public void setIdtabelaauditoria(int idtabelaauditoria) {
        this.idtabelaauditoria = idtabelaauditoria;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idauditoria != null ? idauditoria.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Auditorias)) {
            return false;
        }
        Auditorias other = (Auditorias) object;
        if ((this.idauditoria == null && other.idauditoria != null) || (this.idauditoria != null && !this.idauditoria.equals(other.idauditoria))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Auditorias[ idauditoria=" + idauditoria + " ]";
    }
    
}
