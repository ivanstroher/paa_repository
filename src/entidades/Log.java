/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ivan.stroher
 */
@Entity
@Table(name = "log")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Log.findAll", query = "SELECT l FROM Log l"),
    @NamedQuery(name = "Log.findByIdlog", query = "SELECT l FROM Log l WHERE l.idlog = :idlog"),
    @NamedQuery(name = "Log.findByDatalog", query = "SELECT l FROM Log l WHERE l.datalog = :datalog"),
    @NamedQuery(name = "Log.findByValor", query = "SELECT l FROM Log l WHERE l.valor = :valor")})
public class Log implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idlog")
    private Integer idlog;
    @Basic(optional = false)
    @Column(name = "datalog")
    private String datalog;
    @Basic(optional = false)
    @Column(name = "valor")
    private String valor;
    @Basic(optional = false)
    @Column(name = "idusuariolog")
    private int idusuariolog;
    @Basic(optional = false)
    @Column(name = "idtabelalog")
    private int idtabelalog;
//    @JoinColumn(name = "idusuariolog", referencedColumnName = "idusuario")
//    @ManyToOne(optional = false)
//    private Usuarios idusuariolog;
//    @JoinColumn(name = "idtabelalog", referencedColumnName = "idtabela")
//    @ManyToOne(optional = false)
//    private Tabelaauditoria idtabelalog;

    public Log() {
    }

    public Log(Integer idlog) {
        this.idlog = idlog;
    }

    public Log(Integer idlog, String datalog, String valor) {
        this.idlog = idlog;
        this.datalog = datalog;
        this.valor = valor;
    }

    public Integer getIdlog() {
        return idlog;
    }

    public void setIdlog(Integer idlog) {
        this.idlog = idlog;
    }

    public String getDatalog() {
        return datalog;
    }

    public void setDatalog(String datalog) {
        this.datalog = datalog;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public int getIdusuariolog() {
        return idusuariolog;
    }

    public void setIdusuariolog(int idusuariolog) {
        this.idusuariolog = idusuariolog;
    }

    public int getIdtabelalog() {
        return idtabelalog;
    }

    public void setIdtabelalog(int idtabelalog) {
        this.idtabelalog = idtabelalog;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idlog != null ? idlog.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Log)) {
            return false;
        }
        Log other = (Log) object;
        if ((this.idlog == null && other.idlog != null) || (this.idlog != null && !this.idlog.equals(other.idlog))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Log[ idlog=" + idlog + " ]";
    }
    
}
