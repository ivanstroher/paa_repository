/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidades;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ivan.stroher
 */
@Entity
@Table(name = "vwvendas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vwvendas.findAll", query = "SELECT v FROM Vwvendas v"),
    @NamedQuery(name = "Vwvendas.findByDescricao", query = "SELECT v FROM Vwvendas v WHERE v.descricao = :descricao"),
    @NamedQuery(name = "Vwvendas.findByIdevento", query = "SELECT v FROM Vwvendas v WHERE v.idevento = :idevento"),
    @NamedQuery(name = "Vwvendas.findByDataevento", query = "SELECT v FROM Vwvendas v WHERE v.dataevento = :dataevento"),
    @NamedQuery(name = "Vwvendas.findByHoraevento", query = "SELECT v FROM Vwvendas v WHERE v.horaevento = :horaevento"),
    @NamedQuery(name = "Vwvendas.findByLocal", query = "SELECT v FROM Vwvendas v WHERE v.local = :local"),
    @NamedQuery(name = "Vwvendas.findByDatavenda", query = "SELECT v FROM Vwvendas v WHERE v.datavenda = :datavenda"),
    @NamedQuery(name = "Vwvendas.findBySetor", query = "SELECT v FROM Vwvendas v WHERE v.setor = :setor"),
    @NamedQuery(name = "Vwvendas.findByVendidos", query = "SELECT v FROM Vwvendas v WHERE v.vendidos = :vendidos"),
    @NamedQuery(name = "Vwvendas.findByValortotal", query = "SELECT v FROM Vwvendas v WHERE v.valortotal = :valortotal")})
public class Vwvendas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "idevento")
    private Integer idevento;
    @Column(name = "descricao")
    private String descricao;
    @Column(name = "dataevento")
    private String dataevento;
    @Column(name = "horaevento")
    private String horaevento;
    @Column(name = "Local")
    private String local;
    @Column(name = "datavenda")
    @Temporal(TemporalType.DATE)
    private Date datavenda;
    @Column(name = "Setor")
    private String setor;
    @Column(name = "vendidos")
    private BigInteger vendidos;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valortotal")
    private Double valortotal;

    public Vwvendas() {
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getIdevento() {
        return idevento;
    }

    public void setIdevento(Integer idevento) {
        this.idevento = idevento;
    }

    public String getDataevento() {
        return dataevento;
    }

    public void setDataevento(String dataevento) {
        this.dataevento = dataevento;
    }

    public String getHoraevento() {
        return horaevento;
    }

    public void setHoraevento(String horaevento) {
        this.horaevento = horaevento;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public Date getDatavenda() {
        return datavenda;
    }

    public void setDatavenda(Date datavenda) {
        this.datavenda = datavenda;
    }

    public String getSetor() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }

    public BigInteger getVendidos() {
        return vendidos;
    }

    public void setVendidos(BigInteger vendidos) {
        this.vendidos = vendidos;
    }

    public Double getValortotal() {
        return valortotal;
    }

    public void setValortotal(Double valortotal) {
        this.valortotal = valortotal;
    }
    
}
